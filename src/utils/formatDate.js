const formatDate = (date) => {
  const formattedDate = new Date(date).toLocaleDateString(
    {},
    { timeZone: "UTC", month: "long", day: "2-digit", year: "numeric" }
  );
  const sp = formattedDate.split(" ");
  return `${sp[1]} ${sp[0]}, ${sp[2]}`;
};

export default formatDate;
