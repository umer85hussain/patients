const { Selector } = require("testcafe");
const { When, Then } = require('@cucumber/cucumber');
const patientmanagementPage = require('../../support/pages/patient-management')

When("Carmen removes a patient", async function() {
    await testController
        .click(patientmanagementPage.patientmanagement.toggleMenu())
        .click(patientmanagementPage.patientmanagement.toggleArchive());
});

Then("All patient records for the patient are removed", async function() {

});