import React, { useReducer, useEffect, useState, useCallback } from "react";

import TitleBar from "../../components/TitleBar";
import PatientTabs from "../../components/Tabs";
import PatientService from "../../api/patient";
import { initialPatients, patientsReducer } from "../../reducers/patients";

const Dashboard = () => {
  const [state, dispatch] = useReducer(patientsReducer, initialPatients);
  const [update, updateState] = useState();
  const forceUpdate = useCallback(() => updateState({}), []);

  useEffect(() => {
    PatientService.getPatients().then((result) =>
      dispatch({ type: "SET_PATIENTS", patient: result })
    );
  }, [update]);

  return (
    <div className="rootWrapper">
      <TitleBar forceUpdate={forceUpdate} state={state} />

      <PatientTabs state={state} forceUpdate={forceUpdate} />
    </div>
  );
};

export default Dashboard;
