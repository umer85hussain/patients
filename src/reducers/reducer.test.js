import { patientsReducer } from "./patients";

it('returns new state for patient', () => {
  const initialState = [];
  const updateAction = { type: 'SET_PATIENTS', patient: { name: "test patient" } };
  const updatedState = patientsReducer(initialState, updateAction);
  expect(updatedState).toEqual({ patients: { name: "test patient" } });
  const state = patientsReducer(updatedState, { type: '', patient: {} })
  expect(state).toEqual({ patients: { name: "test patient" } });
});