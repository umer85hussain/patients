import React from "react";
import { render } from "@testing-library/react";
import Dashboard from "./index";

it("test dashboard", () => {
  const { container } = render(<Dashboard />);
  expect(container.firstChild).toHaveClass('rootWrapper')
});
