import React, { useState, useEffect, useRef } from "react";
import FormGroupInput from "../FormGroupInput";
import styles from "../UpdateUserInfo/updateUserInfo.module.scss";
import fillAddress from "./fillAddress";

let autoComplete;

const GuardianSearchLocationInput = ({ value, updateAddress, user }) => {
  const [address, setAddress] = useState({});
  const [query, setQuery] = useState(value);
  const autoCompleteRef = useRef(null);

  const handleScriptLoad = async (updateQuery, autoCompleteRef) => {
    autoComplete = new window.google.maps.places.Autocomplete(
      autoCompleteRef.current,
      {
        types: ["geocode"],
      }
    );

    autoComplete.setFields(["address_components", "formatted_address"]);
    autoComplete.addListener("place_changed", () => {
      handlePlaceSelect(updateQuery);
    });
  };

  const handlePlaceSelect = (updateQuery) => {
    const addressObject = autoComplete.getPlace();

    const query = addressObject.formatted_address;
    updateQuery(query);

    setAddress(fillAddress(addressObject.address_components));
  };

  useEffect(() => {
    handleScriptLoad(setQuery, autoCompleteRef);
  }, []);

  useEffect(() => {
    updateAddress(address.street, "", "street");
    updateAddress(address.streetNo, "", "streetNo");
    updateAddress(address.postalCode, "", "postalCode");
    updateAddress(address.country, "", "country");
    updateAddress(address.city, "", "city");
  }, [address]);

  return (
    <FormGroupInput
      id="inputFieldLegalGuardianStreet"
      displayText="Street"
      autoCompleteRef={autoCompleteRef}
      style={styles.customInput}
      handleChange={(value) => {
        updateAddress(value, user, "street");
        setQuery(value);
      }}
      value={value}
      disable={false}
    />
  );
};

export default GuardianSearchLocationInput;
