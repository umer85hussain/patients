import React, { useState } from "react";

import UpdateUserInfo from "../../components/UpdateUserInfo";
import { emptyPatient } from "../../constants";
import styles from "./noPatient.module.scss";
import plusicon from "./images/plusIcon.svg";

const NoPatient = ({ forceUpdate }) => {
  const plusIcon = plusicon;
  const [modal, setModal] = useState(false);
  const [nestedModal, setNestedModal] = useState(false);

  const toggle = () => setModal(!modal);
  const nestedToggle = () => setNestedModal(!nestedModal);

  return (
    <div className={styles.rootWrapper}>
      <div className={styles.titleWarpper}>NO PATIENTS</div>
      <div className={styles.headingWrapper}>Create new patient</div>
      <div className={styles.buttonWrapper} onClick={() => setModal(true)}>
        <div className={styles.plusWrapper}>
          <div className={styles.plus}>
            <img src={plusIcon} alt="+" className={styles.plusIcon} />
          </div>
        </div>
        <button className={styles.newBtn}> New Patient</button>
        <UpdateUserInfo
          modal={modal}
          toggle={toggle}
          nestedModal={nestedModal}
          setNestedModal={setNestedModal}
          nestedToggle={nestedToggle}
          patient={emptyPatient}
          forceUpdate={forceUpdate}
        />
      </div>
    </div>
  );
};

export default NoPatient;
