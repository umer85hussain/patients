import request from "../request";

function getPatient(id) {
  return request({
    url: `/people/${id}`,
    method: "GET",
  });
}

function getPatients() {
  return request({
    url: `/people`,
    method: "GET",
  });
}

function addPatient(patientDetail) {
  return request({
    url: "/people",
    method: "POST",
    data: patientDetail,
  });
}

function updatePatient(patientDetail) {
  return request({
    url: `/people/${patientDetail.id}`,
    method: "PUT",
    data: patientDetail,
  });
}

function updatePatientStatus(id, status) {
  return request({
    url: `/patients/${id}/status`,
    method: "PUT",
    data: status,
  });
}

function archivePatient(id) {
  return request({
    url: `/patients/${id}/archive`,
    method: "PUT",
  });
}

const PatientService = {
  getPatient,
  getPatients,
  addPatient,
  archivePatient,
  updatePatient,
  updatePatientStatus
};

export default PatientService;
