import React from "react";
import { render } from "@testing-library/react";
import UpdateStatus from "./index";

it("test patient status update", () => {
  const { container } = render(
    <UpdateStatus
      people={{ archived: false, patients: [{archived: false}] }}
      role={{ value: "NOT_PRESENT", label: "Not Present" }}
    />
  );
  expect(container.firstChild).toHaveClass("css-2b097c-container");
});
