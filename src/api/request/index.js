import axios from "axios";
import { BASE_API_URL } from "../../constants";

const client = axios.create({
  baseURL: BASE_API_URL,
  // headers: {
  //   'Access-Control-Allow-Origin' : 'http://localhost:8080',
  // }
});

// client.interceptors.request.use(function (config) {
//   config.headers.Authorization =  `Bearer ${process.env.REACT_APP_BEARER_TOKEN}`;
//   return config;
// });

client.interceptors.request.use(config => {
  config.headers.put['Content-Type'] = 'text/plain;charset=UTF-8';
  return config;
});

const request = function (options) {
  const onSuccess = function (response) {
    console.debug("Request Successful!", response);
    return response.data;
  };

  const onError = function (error) {
    console.error("Request Failed:", error.config);

    if (error.response) {
      console.error("Status:", error.response.status);
      console.error("Data:", error.response.data);
      console.error("Headers:", error.response.headers);
    } else {
      console.error("Error Message:", error.message);
    }

    throw new Error(error.message);
  };

  return client(options).then(onSuccess).catch(onError);
};

export default request;
