import React from "react";
import PropTypes from "prop-types";

import Select from "../Select";
import { SELECT_ROLE_STYLE, STATUS } from "../../constants";
import PatientService from "../../api/patient";

const UpdateRole = ({ role, people, forceUpdate }) => {
  const handleChange = (id, value) => {
    PatientService.updatePatientStatus(id, value).then(() => {
      forceUpdate();
    });
  };

  return (
    <Select
      disable={
        people.patients && people.patients.length && people.patients[0].archived
      }
      options={STATUS}
      value={role.value}
      handleChange={(status) =>
        handleChange(people.patients[0].id, status.value)
      }
      style={SELECT_ROLE_STYLE}
      label={role.label}
    />
  );
};

UpdateRole.propTypes = {
  roll: PropTypes.object,
  label: PropTypes.string,
};

export default UpdateRole;
