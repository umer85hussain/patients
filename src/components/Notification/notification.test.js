import React from "react";
import { render, fireEvent } from "@testing-library/react";
import Notification from ".";

it("test notification components", () => {
  const componentUI = render(
    <>
      <Notification modal={true} status="Success" />
    </>
  );
  componentUI.getByText("Message");
});
