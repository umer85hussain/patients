import React from "react";
import { render } from "@testing-library/react";
import PatientList from "./index";
import list from "./mocks";

it("test patient list table", () => {
  const patientList = [
    {
      firstName: "Franky",
      lastName: "Stiller",
      titel: "Prof.",
      birthdate: "2000-01-23",
      phone: "+49 831 54091090",
      patientSince: "2000-01-23",
      insurence: [
        { name: "AOK", id: 6, type: "private" },
        { name: "AOK", id: 6, type: "private" },
      ],
      id: 0,
      job: "Manager",
      lastAppointment: "2000-01-23",
      insurenceId: "BZT301381240",
      eMail: "franky.stiller@test.com",
      addresses: [
        {
          street: "Kottenerstr.",
          streetNo: "80",
          postalCode: "87435",
          city: "Kempten",
          country: "Germany",
        },
      ],
    },
  ];

  const { getByText } = render(<PatientList patients={patientList} />);
  getByText("Patient");
  getByText("Address");
  getByText("Date of Birth");
  getByText("Appointment date");
  getByText("Status");
  getByText("Franky Stiller");
  getByText("20 years");
});

describe("mocks", () => {
  test("test list import success", () => {
    expect(list);
  });
});
