const { Selector } = require("testcafe");
const { When } = require("@cucumber/cucumber");
const patientmanagementPage = require("../../support/pages/patient-management");

When(
  "Carmen adds a new legal guardian to the patient with valid data",
  async function () {
    await testController.click(
      patientmanagementPage.patientmanagement.inputLegalGuardianLabel()
    );

    await testController
      .typeText(
        patientmanagementPage.patientmanagement.inputFieldLegalGuardianName(),
        "John Doe"
      )
      .expect(
        patientmanagementPage.patientmanagement.inputFieldLegalGuardianName()
          .value
      )
      .eql("John Doe");

    await testController
      .typeText(
        patientmanagementPage.patientmanagement.inputFieldLegalGuardianStreet(),
        "WC Road"
      )
      .expect(
        patientmanagementPage.patientmanagement.inputFieldLegalGuardianStreet()
          .value
      )
      .eql("WC Road");

    await testController
      .typeText(
        patientmanagementPage.patientmanagement.inputFieldLegalGuardianStreetNumber(),
        "20"
      )
      .expect(
        patientmanagementPage.patientmanagement.inputFieldLegalGuardianStreetNumber()
          .value
      )
      .eql("20");

    await testController
      .typeText(
        patientmanagementPage.patientmanagement.inputFieldLegalGuardianPostalCode(),
        "458771"
      )
      .expect(
        patientmanagementPage.patientmanagement.inputFieldLegalGuardianPostalCode()
          .value
      )
      .eql("458771");

    await testController
      .typeText(
        patientmanagementPage.patientmanagement.inputFieldLegalGuardianCity(),
        "Dr John Doe"
      )
      .expect(
        patientmanagementPage.patientmanagement.inputFieldLegalGuardianCity()
          .value
      )
      .eql("Dr John Doe");

    await testController
      .typeText(
        patientmanagementPage.patientmanagement.inputFieldLegalGuardianCountry(),
        "Brazil"
      )
      .expect(
        patientmanagementPage.patientmanagement.inputFieldLegalGuardianCountry()
          .value
      )
      .eql("Brazil");

    await testController
      .typeText(
        patientmanagementPage.patientmanagement.inputFieldLegalGuardianPhone(),
        "9265865320"
      )
      .expect(
        patientmanagementPage.patientmanagement.inputFieldLegalGuardianPhone()
          .value
      )
      .eql("9265865320");

    await testController
      .typeText(
        patientmanagementPage.patientmanagement.inputFieldLegalGuardianEmail(),
        "johnDoe@gmail.com"
      )
      .expect(
        patientmanagementPage.patientmanagement.inputFieldLegalGuardianEmail()
          .value
      )
      .eql("johnDoe@gmail.com");
  }
);
