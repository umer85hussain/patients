import React from "react";
import { render, fireEvent } from "@testing-library/react";
import TitleBar from "./index";
import Mocks from "../PatientList/mocks";
import { mockGoogle } from "../../utils/mockGoogle";

window.google = global.google = mockGoogle();

it("test titlebar text", () => {
  const { getByText } = render(<TitleBar state={{ patients: Mocks }} />);
  getByText("Patients");
  getByText("New Patient");
});

it("test new patient button click", () => {
  const { getByRole } = render(<TitleBar state={{ patients: Mocks }} />);
  const link = getByRole("button", { name: "New Patient" });
  fireEvent.click(link);
});
