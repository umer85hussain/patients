const { Selector } = require("testcafe");
const { Given, When, Then } = require("@cucumber/cucumber");
const patientmanagementPage = require("../../support/pages/patient-management");

Given("Carmen is logged in", async function () {
  await testController.navigateTo(
    patientmanagementPage.patientmanagement.url()
  );

  await testController.wait(4000);

  await testController
    .expect(patientmanagementPage.patientmanagement.rootElement().exists)
    .ok();
});

Given("There are existing patients", async function () {
  await testController
    .wait(4000)
    .expect(patientmanagementPage.patientmanagement.patientTable().count)
    .gt(0);
});

When("Carmen adds a new patient with valid data", async function () {
  await testController.wait(4000);

  await testController.click(
    patientmanagementPage.patientmanagement.addButton()
  );

  await testController
    .click(patientmanagementPage.patientmanagement.selectGender())
    .pressKey("enter");

  await testController
    .typeText(
      patientmanagementPage.patientmanagement.inputFieldName(),
      "Peter Parker"
    )
    .expect(patientmanagementPage.patientmanagement.inputFieldName().value)
    .eql("Peter Parker");

  await testController
    .typeText(
      patientmanagementPage.patientmanagement.inputFieldBirthDay(),
      "01"
    )
    .expect(patientmanagementPage.patientmanagement.inputFieldBirthDay().value)
    .eql("01");

  await testController
    .click(patientmanagementPage.patientmanagement.inputFieldBirthMonth())
    .pressKey("enter");

  await testController
    .typeText(
      patientmanagementPage.patientmanagement.inputFieldBirthYear(),
      "1996"
    )
    .expect(patientmanagementPage.patientmanagement.inputFieldBirthYear().value)
    .eql("1996");

  await testController
    .typeText(
      patientmanagementPage.patientmanagement.inputFieldStreet(),
      "Bahnhofsstr"
    )
    .expect(patientmanagementPage.patientmanagement.inputFieldStreet().value)
    .eql("Bahnhofsstr");

  await testController
    .typeText(
      patientmanagementPage.patientmanagement.inputFieldStreetNumber(),
      "66"
    )
    .expect(
      patientmanagementPage.patientmanagement.inputFieldStreetNumber().value
    )
    .eql("66");

  await testController
    .typeText(
      patientmanagementPage.patientmanagement.inputFieldPostalCode(),
      "87435"
    )
    .expect(
      patientmanagementPage.patientmanagement.inputFieldPostalCode().value
    )
    .eql("87435");

  await testController
    .typeText(
      patientmanagementPage.patientmanagement.inputFieldCity(),
      "Kempten"
    )
    .expect(patientmanagementPage.patientmanagement.inputFieldCity().value)
    .eql("Kempten");

  await testController
    .typeText(
      patientmanagementPage.patientmanagement.inputFieldCountry(),
      "Germany"
    )
    .expect(patientmanagementPage.patientmanagement.inputFieldCountry().value)
    .eql("Germany");

  await testController
    .typeText(
      patientmanagementPage.patientmanagement.inputFieldPhone(),
      "49123456789"
    )
    .expect(patientmanagementPage.patientmanagement.inputFieldPhone().value)
    .eql("49123456789");

  await testController
    .typeText(
      patientmanagementPage.patientmanagement.inputFieldEmail(),
      "test@email.com"
    )
    .expect(patientmanagementPage.patientmanagement.inputFieldEmail().value)
    .eql("test@email.com");

  await testController
    .typeText(
      patientmanagementPage.patientmanagement.inputFieldProfession(),
      "Tester"
    )
    .expect(
      patientmanagementPage.patientmanagement.inputFieldProfession().value
    )
    .eql("Tester");

  await testController
    .typeText(
      patientmanagementPage.patientmanagement.inputFieldFamilyDoctor(),
      "Dr John Doe"
    )
    .expect(
      patientmanagementPage.patientmanagement.inputFieldFamilyDoctor().value
    )
    .eql("Dr John Doe");

  await testController
    .click(patientmanagementPage.patientmanagement.inputFieldCareLevel())
    .pressKey("enter");

  await testController
    .click(patientmanagementPage.patientmanagement.inputInsuranceLabel())
    .typeText(
      patientmanagementPage.patientmanagement.inputFieldLegalInsuranceNumber(),
      "N413445234"
    )
    .expect(
      patientmanagementPage.patientmanagement.inputFieldLegalInsuranceNumber()
        .value
    )
    .eql("N413445234");
});

When("Carmen saves the patient", async function () {
  await testController.click(
    patientmanagementPage.patientmanagement.createButton()
  );
});

Then("Carmen sees the new patient in the overview", async function () {
  await testController
    .expect(patientmanagementPage.patientmanagement.tableEntries().exists)
    .ok();
});
