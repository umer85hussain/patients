export const initialPatients = {
  patients: [],
};

export const patientsReducer = (state, action) => {
  switch (action.type) {
    case "SET_PATIENTS":
      return { ...state, patients: action.patient };
    default:
      return state;
  }
};
