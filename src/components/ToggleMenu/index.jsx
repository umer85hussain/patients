import React, { useState } from "react";
import PropTypes from "prop-types";

import styles from "./toggleMenu.module.scss";
import OutsideAlerter from "../OutsideClickHandler";

const ToggleMenu = ({ items }) => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  const handleItemClick = (item) => {
    setIsMenuOpen(false);
    item.handleClick();
  };

  return (
    <OutsideAlerter actionHandler={() => setIsMenuOpen(false)}>
      <div
        className={styles.dots}
        onClick={() => setIsMenuOpen(!isMenuOpen)}
      ></div>
      {!!isMenuOpen && (
        <div className={styles.itemGroup}>
          {items.map((item) => (
            <div onClick={() => handleItemClick(item)} className={styles.item}>
              <img src={item.logo} alt="icon" />
              <div className={styles.label}>{item.label}</div>
            </div>
          ))}
        </div>
      )}
    </OutsideAlerter>
  );
};

ToggleMenu.propTypes = {
  items: PropTypes.array,
};

export default ToggleMenu;
