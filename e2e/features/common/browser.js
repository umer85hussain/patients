const { By, until } = require("selenium-webdriver");
const { timeout } = require("../../config");

function browser(driver) {
  function waitAndLocateByCSS(selector) {
    return driver.wait(until.elementLocated(By.css(selector)), timeout);
  }

  function findByClassName(selector){
    return driver.findElement(By.className(selector));
  }

  function findById(selector){
    return driver.findElement(By.id(selector));
  }

  function waitAndLocateByXpath(selector) {
    return driver.wait(until.elementLocated(By.xpath(selector)), timeout);
  }

  return {
    waitAndLocateByCSS,
    waitAndLocateByXpath,
    findByClassName,
    findById
  };
}

module.exports = browser;
