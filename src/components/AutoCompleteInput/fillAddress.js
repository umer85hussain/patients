const fillAddress = (addressObject) => {
  //fetch Street
  const sublocality_level_1 = addressObject.filter((address) => {
    return address.types[0] === "sublocality_level_1";
  });
  const sublocality_level_2 = addressObject.filter((address) => {
    return address.types[0] === "sublocality_level_2";
  });
  const sublocality_level_3 = addressObject.filter((address) => {
    return address.types[0] === "sublocality_level_3";
  });
  const route = addressObject.filter((address) => {
    return address.types[0] === "route";
  });
  //Street string construction
  let street =
    sublocality_level_1.length && sublocality_level_1[0].long_name
      ? sublocality_level_1[0].long_name
      : "";
  if (sublocality_level_2.length && sublocality_level_2[0].long_name) {
    if (street.length) {
      street = street + ", " + sublocality_level_2[0].long_name;
    } else {
      street = sublocality_level_2[0].long_name;
    }
  }
  if (sublocality_level_3.length && sublocality_level_3[0].long_name) {
    if (street.length) {
      street = street + ", " + sublocality_level_3[0].long_name;
    } else {
      street = sublocality_level_3[0].long_name;
    }
  }
  if (route.length && route[0].long_name) {
    if (street.length) {
      street = street + ", " + route[0].long_name;
    } else {
      street = route[0].long_name;
    }
  }

  //fetch street number
  const street_number = addressObject.filter((address) => {
    return address.types[0] === "street_number";
  });
  let streetNo = street_number.length && street_number[0].long_name;

  //fetch city
  const locality = addressObject.filter((address) => {
    return (
      address.types[0] === "locality" ||
      address.types[0] === "administrative_area_level_2" ||
      (address &&
        address.type &&
        address.type.length > 1 &&
        address.type[1] === "locality")
    );
  });

  //fetch state
  const administrative_area_level_1 = addressObject.filter((address) => {
    return address.types[0] === "administrative_area_level_1";
  });
  const state =
    administrative_area_level_1.length &&
    administrative_area_level_1[0].long_name
      ? administrative_area_level_1[0].long_name
      : "";

  //City string construction
  let city =
    locality.length && locality[0].long_name ? locality[0].long_name : "";
  if (
    administrative_area_level_1.length &&
    administrative_area_level_1[0].long_name
  ) {
    if (city.length) {
      city = city + ", " + administrative_area_level_1[0].long_name;
    } else {
      street = administrative_area_level_1[0].long_name;
    }
  }
  //fetch country
  const country = addressObject.filter((address) => {
    return address.types[0] === "country";
  });

  //fetch postal code
  const postalCode = addressObject.filter((address) => {
    return address.types[0] === "postal_code";
  });

  return {
    street: street.length ? street : "",
    streetNo: streetNo.length ? streetNo : "",
    city: city.length ? city : "",
    state: state.length ? state : "",
    country: country.length && country[0].long_name ? country[0].long_name : "",
    postalCode:
      postalCode.length && postalCode[0].long_name
        ? postalCode[0].long_name
        : "",
  };
};
export default fillAddress;
