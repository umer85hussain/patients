import React, { useState } from "react";

import UpdateUserInfo from "../UpdateUserInfo";
import { emptyPatient } from "../../constants";
import styles from "./titleBar.module.scss";

const TitleBar = ({ state, forceUpdate }) => {
  const [modal, setModal] = useState(false);
  const [nestedModal, setNestedModal] = useState(false);

  const toggle = () => setModal(!modal);
  const nestedToggle = () => setNestedModal(!nestedModal);

  return (
    <>
      {!!(state && state.patients && state.patients.length) && (
        <div className={styles.rootWrapper}>
          <div className={styles.title}>Patients</div>
          <div className={styles.buttonWrapper} onClick={() => setModal(true)}>
            <div className={styles.plusWrapper}>
              <div className={styles.plus}>+</div>
            </div>
            <button
              className={styles.newBtn}
            >
              {" "}
              New Patient
            </button>
            <UpdateUserInfo
              forceUpdate={forceUpdate}
              modal={modal}
              toggle={toggle}
              nestedModal={nestedModal}
              setNestedModal={setNestedModal}
              nestedToggle={nestedToggle}
              patient={emptyPatient}
            />
          </div>
        </div>
      )}
    </>
  );
};

export default TitleBar;
