import React from "react";
import PropTypes from 'prop-types';
import { TabPane, Row, Col } from "reactstrap";

const TabPanel = ({ navIndex, children }) => {
  return (
    <TabPane tabId={navIndex}>
      <Row>
        <Col sm="12">{children}</Col>
      </Row>
    </TabPane>
  );
};

TabPane.propTypes = {
  navIndex: PropTypes.string,
  children: PropTypes.object
}

export default TabPanel;
