const mocks = [{
  gender: "Ms.",
  firstName: "Julia",
  lastName: "Maier",
  email: null,
  phone: "+49 0831 123",
  profession: "Student",
  birthdate: "2010-05-23T16:23:27.868475+02:00",
  archived: false,
  legalGuardians: [
    {
      firstName: "Moritz",
      lastName: "Maier",
      email: "moritz@maier.de",
      phone: "+49 0821 123",
      archived: false,
      legalGuardians: [],
      patients: [],
      addresses: [
        {
          street: "Kottenerstr.",
          streetNo: "80",
          postalCode: "87435",
          city: "Kempten",
          country: "Germany",
          archived: false,
        },
      ],
    },
  ],
  patients: [
    {
      insuranceNumber: "ABC12345",
      patientSince: "2020-10-15T20:14:04.636472+02:00",
      lastAppointment: null,
      status: "NOT_PRESENT",
      careLevel: null,
      familyDoctor: "Dr. Maier",
      archived: false,
      notes: [],
      images: [],
      insurance: {
        id: 1,
        type: "LEGAL",
        archived: false,
      },
      ward: false,
    },
  ],
  addresses: [
    {
      street: "Kottenerstr.",
      streetNo: "80",
      postalCode: "87435",
      city: "Kempten",
      country: "Germany",
      archived: false,
    },
  ],
}];

export default mocks;
