import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import {
  Modal,
  ModalHeader,
  ModalBody,
  Button,
  Form,
  FormGroup,
  Label,
  Row,
  Col,
} from "reactstrap";
import cx from "classnames";

import styles from "./updateUserInfo.module.scss";
import {
  TITLE_OPTIONS,
  MONTH_OPTIONS,
  DEGREE_OF_CARE_OPTIONS,
} from "../../constants";
import PatientService from "../../api/patient";
import Notification from "../Notification";
import NestedModal from "../NestedModal";
import DefaultImage from "../DefaultImage";
import FormGroupInput from "../FormGroupInput";
import SelectMenu from "../DropdownMenu";
import SearchLocationInput from "../AutoCompleteInput/SearchLocationInput";
import GuardianSearchLocationInput from "../AutoCompleteInput/GuardianSearchLocationInput";
import {
  isAlphaNumStr,
  isValidDay,
  isValidEmailAddress,
  isValidName,
  isValidPhoneNumber,
  isValidPostalCode,
  isValidYear,
} from "../../utils/validations";
import { checkRequiredFields } from "./checkRequiredField";

const Cameraimg =
  "https://storage.googleapis.com/dechea-microservices-files/patient-management/cam.svg";
const lock =
  "https://storage.googleapis.com/dechea-microservices-files/patient-management/lock.svg";
const globe =
  "https://storage.googleapis.com/dechea-microservices-files/patient-management/globe.svg";
const User =
  "https://storage.googleapis.com/dechea-microservices-files/patient-management/user.svg";
const whitecircletick =
  "https://storage.googleapis.com/dechea-microservices-files/patient-management/whitecircletick.svg";

const UpdateUserInfo = ({
  modal,
  toggle,
  nestedModal,
  nestedToggle,
  setNestedModal,
  patient,
  isDisabled,
  setDisableFields = () => "",
  forceUpdate,
  modalId,
}) => {
  const [name, setName] = useState(null);
  const [title, setTitle] = useState({ value: null, label: null });
  const [phone, setPhone] = useState(null);
  const [email, setEmail] = useState(null);
  const [insuranceId, setInsuranceId] = useState(null);
  const [insuranceNumber, setInsuranceNumber] = useState(null);
  const [address, setAddress] = useState({});
  const [isLegalGuardian, setIsLegalGuardian] = useState(false);
  const [birthdate, setBirthDate] = useState();
  const [birthMonth, setBirthMonth] = useState();
  const [birthYear, setbirthYear] = useState();
  const [notificationModal, setNotificationModal] = useState(false);
  const [requestStatus, setRequestStatus] = useState(true);
  const [radioInput, setRadioInput] = useState("");
  const [profession, setProfession] = useState(null);
  const [familyDoctor, setFamilyDoctor] = useState(null);
  const [degreeOfCare, setDegreeOfCare] = useState({
    value: null,
    label: null,
  });
  const [guardianName, setGuardianName] = useState("");
  const [guardianEmail, setGuardianEmail] = useState("");
  const [guardianPhone, setGuardianPhone] = useState("");
  const [guardianAddress, setGuardianAddress] = useState({});
  const [requiredError, setRequiredError] = useState("");

  useEffect(() => {
    setRequiredError(null);
  }, [
    name,
    title,
    phone,
    email,
    insuranceNumber,
    address,
    isLegalGuardian,
    birthdate,
    birthMonth,
    birthYear,
    profession,
    familyDoctor,
    degreeOfCare,
  ]);

  const getError = (field) => {
    if (requiredError && requiredError.length) {
      const res = requiredError.filter(
        (error) => Object.keys(error)[0] === field
      );
      return res[0];
    }
    return null;
  };

  useEffect(() => {
    const patientListLengthCheck =
      patient && patient.patients && patient.patients.length;
    const patientLegalGuardianLengthCheck =
      patient && patient.legalGuardians && patient.legalGuardians.length;

    setName(patient.firstName && `${patient.firstName} ${patient.lastName}`);
    setTitle(patient && { value: patient.gender, label: patient.gender });
    setPhone(patient && patient.phone);
    setEmail(patient && patient.email);
    setInsuranceId(patientListLengthCheck && patient.patients[0].insurance.id);
    setInsuranceNumber(
      patientListLengthCheck && patient.patients[0].insuranceNumber
    );
    setProfession(patient && patient.profession);
    setFamilyDoctor(patientListLengthCheck && patient.patients[0].familyDoctor);
    setDegreeOfCare(
      patient && {
        value: patientListLengthCheck && patient.patients[0].careLevel,
        label: patientListLengthCheck && patient.patients[0].careLevel,
      }
    );

    isDisabled &&
      (patientListLengthCheck && patient.patients[0].insurance.id === 2
        ? setRadioInput("private")
        : setRadioInput("legal"));

    if (patient && patient.birthdate) {
      const date = new Date(patient.birthdate);
      setBirthDate(date.getDate());
      MONTH_OPTIONS.forEach((month) => {
        if (month.value === date.getMonth() + 1)
          setBirthMonth({ value: date.getMonth() + 1, label: month.label });
      });
      setbirthYear(date.getFullYear());
    }
    setAddress(patient.addresses && patient.addresses[0]);
    setGuardianName(
      patientLegalGuardianLengthCheck &&
        patient.legalGuardians[0].firstName &&
        `${patient.legalGuardians[0].firstName} ${patient.legalGuardians[0].lastName}`
    );
    setGuardianEmail(
      patientLegalGuardianLengthCheck && patient.legalGuardians[0].email
    );
    setGuardianPhone(
      patientLegalGuardianLengthCheck && patient.legalGuardians[0].phone
    );
    setGuardianAddress(
      patientLegalGuardianLengthCheck &&
        patient.legalGuardians[0].addresses.length &&
        patient.legalGuardians[0].addresses[0]
    );
    isDisabled && setIsLegalGuardian(patientLegalGuardianLengthCheck);
  }, [isDisabled, patient]);

  const checkAddress = (existingValue, input) => {
    return existingValue &&
      input &&
      existingValue.streetNo === input.streetNo &&
      existingValue.street === input.street &&
      existingValue.postalCode === input.postalCode &&
      existingValue.city === input.city &&
      existingValue.country === input.city
      ? true
      : false;
  };

  const handleClose = () => {
    const date = new Date(patient.birthdate);
    const patientAddress = patient.addresses[0];
    const patientLegalGuardian = patient.legalGuardians[0];
    const patientLegalGuardianAddress = patientLegalGuardian.addresses[0];
    if (
      title.label === patient.gender &&
      name === `${patient.firstName} ${patient.lastName}` &&
      phone === patient.phone &&
      email === patient.email &&
      insuranceId === patient.patients[0].insurance.id &&
      insuranceNumber === patient.patients[0].insuranceNumber &&
      profession === patient.profession &&
      familyDoctor === patient.patients[0].familyDoctor &&
      degreeOfCare.label === patient.patients[0].careLevel &&
      (birthdate || birthMonth || birthYear
        ? patient.birthdate
          ? birthdate === date.getDate() &&
            birthMonth.value - 1 === date.getMonth() &&
            birthYear === date.getFullYear()
          : false
        : true) &&
      checkAddress(patientAddress, address) &&
      guardianName ===
        `${patientLegalGuardian.firstName} ${patientLegalGuardian.lastName}` &&
      guardianEmail === patientLegalGuardian.email &&
      guardianPhone === patientLegalGuardian.phone &&
      checkAddress(patientLegalGuardianAddress, guardianAddress)
    ) {
      toggle();
      emptyState();
      setDisableFields(true);
    } else {
      setNestedModal(true);
    }
  };

  const updateDate = (day, month, year, birthDate) => {
    let date = null;
    date = new Date();
    if (birthDate) date = new Date(birthDate);
    date.setDate(day);
    date.setMonth(month.value - 1);
    date.setFullYear(year);
    date = date.toISOString();
    return date;
  };

  const handleSubmit = async () => {
    let userFirstName = "",
      userLastName = "";
    if (name && name.length) {
      const nameArray = name.split(" ");
      if (name.includes(" ")) {
        userFirstName = nameArray[0];
        userLastName = nameArray[1];
      } else {
        userFirstName = name;
      }
    }
    const commonFields = {
      gender: title.value,
      firstName: userFirstName || " ",
      lastName: userLastName || " ",
      email,
      phone,
      profession,
      archived: false,
    };
    let guardianFields;
    let guardianFirstName = "",
      guardianLastName = "";
    if (guardianName && guardianName.length) {
      const guardianNameNameArray = guardianName.split(" ");
      if (guardianName.includes(" ")) {
        guardianFirstName = guardianNameNameArray[0];
        guardianLastName = guardianNameNameArray[1];
      } else {
        guardianFirstName = guardianName;
      }
    }
    if (guardianName && guardianName.length) {
      guardianFields = {
        id: patient.legalGuardians[0].id,
        firstName: guardianFirstName || " ",
        lastName: guardianLastName || " ",
        email: guardianEmail,
        phone: guardianPhone,
        archived: false,
        legalGuardians: [],
        patients: [],
        addresses: [{ ...guardianAddress, archived: false }],
      };
    }
    if (address && address["0"]) {
      delete address["0"];
    }
    let birthDate = null;
    let currentDate = new Date();
    currentDate = currentDate.toISOString();
    if (patient.id) {
      try {
        if (patient.birthdate && birthYear && birthdate && birthMonth) {
          birthDate = updateDate(
            parseInt(birthdate),
            birthMonth,
            parseInt(birthYear),
            patient.birthdate
          );
        }

        const res = await PatientService.updatePatient({
          ...patient,
          ...commonFields,
          birthdate,
          legalGuardians: guardianFields !== undefined ? [guardianFields] : [],
          patients: [
            {
              ...patient.patients[0],
              insuranceNumber,
              patientSince: currentDate,
              lastAppointment: null,
              status: patient.patients[0].status,
              careLevel: degreeOfCare.value,
              familyDoctor,
              archived: false,
              notes: [],
              images: [],
              insurance: {
                id: insuranceId,
                type: "LEGAL",
                archived: false,
              },
              ward: false,
            },
          ],
          addresses: [{ ...address, archived: false }],
        });
        setNotificationModal(true);
        if (res) {
          setRequestStatus("Success");
        } else {
          setRequestStatus("Error");
        }
        setNotificationModal(true);
        setRequestStatus("Success");
      } catch {
        setNotificationModal(true);
        setRequestStatus("Error");
      }
    } else {
      try {
        console.log(birthdate, birthYear, birthMonth);
        if (birthYear && birthdate && birthMonth) {
          birthDate = updateDate(
            parseInt(birthdate),
            birthMonth,
            parseInt(birthYear),
            ""
          );
        }
        const errors = checkRequiredFields({
          ...commonFields,
          birthdate,
          birthMonth,
          birthYear,
          address,
          degreeOfCare,
          familyDoctor,
          insuranceNumber,
        });
        if (errors.length) {
          setRequiredError(errors);
          return;
        }
        await PatientService.addPatient({
          ...commonFields,
          birthdate,
          legalGuardians: guardianFields !== undefined ? [guardianFields] : [],
          patients: [
            {
              insuranceNumber,
              patientSince: currentDate,
              lastAppointment: null,
              status: "WAITROOM",
              careLevel: degreeOfCare.value,
              familyDoctor,
              archived: false,
              notes: [],
              images: [],
              insurance: {
                id: insuranceId,
                type: "LEGAL",
                archived: false,
              },
              ward: false,
            },
          ],
          addresses: [{ ...address, archived: false }],
        });
        setNotificationModal(true);
        setRequestStatus("Success");
      } catch {
        setNotificationModal(true);
        setRequestStatus("Error");
      }
    }
    forceUpdate();
    toggle();
    emptyState();
  };

  const toggleAll = () => {
    nestedToggle();
    toggle();
    emptyState();
  };

  const emptyState = () => {
    setName(
      patient.firstName ? `${patient.firstName} ${patient.lastName}` : ""
    );
    setTitle(patient && { value: patient.title, label: patient.title });
    setPhone(patient && patient.phone);
    setEmail(patient && patient.email);
    setAddress(patient.addresses && patient.addresses[0]);
    setInsuranceId(patient && patient.insuranceId);
    setInsuranceNumber(patient && patient.insuranceNumber);
    setProfession(patient && patient.profession);
    setFamilyDoctor(patient && patient.familyDoctor);
    setDegreeOfCare(
      patient && { value: patient.degreeOfCare, label: patient.degreeOfCare }
    );
    setIsLegalGuardian(false);
    setRadioInput("");
    if (patient.birthdate) {
      const date = new Date(patient.birthdate);
      setBirthDate(date.getDate());
      MONTH_OPTIONS.forEach((month) => {
        if (month.value === date.getMonth() + 1)
          setBirthMonth({ value: date.getMonth() + 1, label: month.label });
      });
      setbirthYear(date.getFullYear());
    } else {
      setBirthDate("");
      setBirthMonth("");
      setbirthYear("");
    }
  };

  const updateAddress = (value, role, field) => {
    if (role === "PATIENT" && address) {
      address[field] = value;
      setAddress({ ...address });
    } else {
      if (guardianAddress) {
        guardianAddress[field] = value;
        setGuardianAddress({ ...guardianAddress });
      }
    }
  };

  return (
    <div className={styles.modalSection} id={modalId}>
      <Modal
        isOpen={modal}
        toggle={handleClose}
        className={styles.modalMain}
        fade={false}
      >
        <ModalHeader className={styles.modalHeader}>
          <div className={styles.mainpopupHeader}>
            {patient.profileImage ? (
              <img
                className={styles.profileImage}
                src="https://i.stack.imgur.com/l60Hf.png"
                alt="profile"
              />
            ) : (
              <DefaultImage className={styles.defImage} />
            )}
            <div className={styles.uploadWrapper}>
              <button className={styles.browseButton}>Upload photo</button>
              <input type="file" name="myfile" />
              <img src={Cameraimg} alt="" />
            </div>
          </div>
        </ModalHeader>
        <ModalBody className={styles.modalBody}>
          <div>
            <NestedModal
              nestedModal={nestedModal}
              nestedToggle={nestedToggle}
              toggleAll={toggleAll}
            />
            <Form>
              <Row className={styles.customRow}>
                <Row className={styles.firstRow}>
                  <Col sm={4}>
                    <FormGroup>
                      <Label className={styles.genderLabel}>Gender:</Label>
                      <SelectMenu
                        id="newGender"
                        options={TITLE_OPTIONS}
                        value={title}
                        handleChange={setTitle}
                        label="Gender"
                        disable={isDisabled}
                        sort={false}
                        requiredError={requiredError && getError("gender")}
                      />
                    </FormGroup>
                  </Col>
                  <Col sm={8} className={styles.nameInput}>
                    <FormGroupInput
                      id="inputFieldName"
                      displayText="Full Name"
                      value={name}
                      style={styles.customInput}
                      handleChange={setName}
                      disable={isDisabled}
                      validate={isValidName}
                      requiredError={
                        requiredError &&
                        (getError("firstName") || getError("lastName"))
                      }
                    />
                  </Col>
                </Row>
                <Label className={styles.customformLabel}>Birth date:</Label>
                <div className={styles.customDate}>
                  <Col sm={3}>
                    <FormGroupInput
                      id="inputFieldBirthDay"
                      displayText="Day"
                      value={birthdate}
                      style={styles.customInput}
                      handleChange={setBirthDate}
                      disable={isDisabled}
                      validate={isValidDay}
                      requiredError={requiredError && getError("birthdate")}
                    />
                  </Col>
                  <Col sm={6} className={styles.birthmonth}>
                    <SelectMenu
                      id="inputFieldBirthMonth"
                      options={MONTH_OPTIONS}
                      value={birthMonth}
                      handleChange={setBirthMonth}
                      label="Month"
                      disable={isDisabled}
                      sort={true}
                      requiredError={requiredError && getError("birthMonth")}
                    />
                  </Col>
                  <Col sm={3}>
                    <FormGroupInput
                      id="inputFieldBirthYear"
                      displayText="Year"
                      value={birthYear}
                      style={styles.customInput}
                      handleChange={setbirthYear}
                      disable={isDisabled}
                      validate={isValidYear}
                      requiredError={requiredError && getError("birthYear")}
                    />
                  </Col>
                </div>
                <Label className={styles.customformLabel}>Address:</Label>
                <Col sm={6}>
                  <SearchLocationInput
                    updateAddress={updateAddress}
                    value={address && address.street}
                    user={"PATIENT"}
                  />
                  {requiredError && getError("street") && (
                    <div className={styles.streetfeedbackForm}>
                      {Object.values(getError("street"))[0]}
                    </div>
                  )}
                </Col>
                <Col sm={6}>
                  <FormGroupInput
                    id="inputFieldStreetNumber"
                    displayText="Street No."
                    value={address && address.streetNo}
                    style={styles.customInput}
                    handleChange={(value) =>
                      updateAddress(value, "PATIENT", "streetNo")
                    }
                    disable={isDisabled}
                    validate={isAlphaNumStr}
                    requiredError={requiredError && getError("streetNo")}
                  />
                </Col>
                <Col sm={6}>
                  <FormGroupInput
                    id="inputFieldPostalCode"
                    displayText="Postal Code"
                    value={address && address.postalCode}
                    style={styles.customInput}
                    handleChange={(value) =>
                      updateAddress(value, "PATIENT", "postalCode")
                    }
                    disable={isDisabled}
                    validate={isValidPostalCode}
                    requiredError={requiredError && getError("postalCode")}
                  />
                </Col>
                <Col sm={6}>
                  <FormGroupInput
                    id="inputFieldCity"
                    displayText="City"
                    value={address && address.city}
                    style={styles.customInput}
                    handleChange={(value) =>
                      updateAddress(value, "PATIENT", "city")
                    }
                    disable={isDisabled}
                    validate={isAlphaNumStr}
                    requiredError={requiredError && getError("city")}
                  />
                </Col>
                <Col sm={12}>
                  <FormGroupInput
                    id="inputFieldCountry"
                    displayText="Country"
                    value={address && address.country}
                    style={styles.customInput}
                    handleChange={(value) =>
                      updateAddress(value, "PATIENT", "country")
                    }
                    disable={isDisabled}
                    validate={isAlphaNumStr}
                    requiredError={requiredError && getError("country")}
                  />
                </Col>
                <Col sm={12}>
                  <FormGroupInput
                    id="inputFieldPhone"
                    displayText="Phone"
                    value={phone}
                    style={styles.customInput}
                    handleChange={setPhone}
                    disable={isDisabled}
                    validate={isValidPhoneNumber}
                    requiredError={requiredError && getError("phone")}
                  />
                </Col>
                <Col sm={12}>
                  <FormGroupInput
                    id="inputFieldEmail"
                    displayText="Email address"
                    value={email}
                    style={styles.customInput}
                    handleChange={setEmail}
                    disable={isDisabled}
                    validate={isValidEmailAddress}
                  />
                </Col>
                <Col sm={12}>
                  <FormGroupInput
                    id="inputFieldProfession"
                    displayText="Profession"
                    value={profession}
                    style={styles.customInput}
                    handleChange={setProfession}
                    disable={isDisabled}
                    validate={isValidName}
                    requiredError={requiredError && getError("profession")}
                  />
                </Col>
                <Col sm={12}>
                  <FormGroupInput
                    id="inputFieldFamilyDoctor"
                    displayText="Family doctor"
                    value={familyDoctor}
                    style={styles.customInput}
                    handleChange={setFamilyDoctor}
                    disable={isDisabled}
                    validate={isValidName}
                    requiredError={requiredError && getError("familyDoctor")}
                  />
                </Col>
                <Label className={styles.customformLabel}>
                  Degree Of care:
                </Label>
                <Col sm={12}>
                  <SelectMenu
                    id="inputFieldCareLevel"
                    options={DEGREE_OF_CARE_OPTIONS}
                    value={degreeOfCare}
                    handleChange={setDegreeOfCare}
                    label="Degree Of Care"
                    disable={isDisabled}
                    sort={false}
                    requiredError={requiredError && getError("degreeOfCare")}
                  />
                </Col>
                <Col sm={12}>
                  <FormGroup>
                    <Label className={styles.formLabel}>Insurance Type:</Label>
                    <div className={styles.blockField}>
                      <p>
                        <input
                          onChange={(event) =>
                            setRadioInput(event.target.id) || setInsuranceId(2)
                          }
                          type="radio"
                          id="private"
                          name="radio-group"
                          default
                          disabled={isDisabled}
                        />
                        <label htmlFor="private" className={styles.labelfield}>
                          Private
                        </label>
                      </p>
                      <img src={lock} alt="lock" />
                    </div>
                    {radioInput === "private" && (
                      <div
                        className={cx(
                          { [styles.fadein]: radioInput },
                          { [styles.fadeout]: !radioInput }
                        )}
                      >
                        <Col sm={13}>
                          <Label className={styles.custominsuredNumber}>
                            Insured Number:
                          </Label>
                          <FormGroupInput
                            id="inputFieldPrivateInsuranceNumber"
                            displayText="Insured number"
                            value={insuranceNumber}
                            style={styles.customInput}
                            handleChange={setInsuranceNumber}
                            disable={isDisabled}
                            validate={isAlphaNumStr}
                          />
                        </Col>
                      </div>
                    )}
                    <div className={styles.blockField}>
                      <p>
                        <input
                          onChange={(event) =>
                            setRadioInput(event.target.id) || setInsuranceId(1)
                          }
                          type="radio"
                          id="legal"
                          name="radio-group"
                          default
                          disabled={isDisabled}
                        />
                        <label htmlFor="legal" className={styles.labelfield}>
                          Legal
                        </label>
                      </p>
                      <img src={globe} alt="" />
                    </div>
                    {requiredError && getError("insuranceNumber") && (
                      <div className={styles.feedbackForm}>
                        {Object.values(getError("insuranceNumber"))[0]}
                      </div>
                    )}
                    {radioInput === "legal" && (
                      <div
                        className={cx(
                          { [styles.fadein]: radioInput },
                          { [styles.fadeout]: !radioInput }
                        )}
                      >
                        <Col sm={13}>
                          <Label className={styles.custominsuredNumber}>
                            Insured Number:
                          </Label>
                          <FormGroupInput
                            id="inputFieldLegalInsuranceNumber"
                            displayText="Insured number"
                            value={insuranceNumber}
                            style={styles.customInput}
                            handleChange={setInsuranceNumber}
                            disable={isDisabled}
                            validate={isAlphaNumStr}
                          />
                        </Col>
                      </div>
                    )}
                  </FormGroup>
                </Col>
                <Col sm={12}>
                  <FormGroup>
                    <div className={styles.blockField}>
                      <p className={styles.squareRadio}>
                        <input
                          type="checkbox"
                          id="legalGuardianCheckbox"
                          disabled={isDisabled}
                          default
                          onChange={() => setIsLegalGuardian(!isLegalGuardian)}
                        />
                        <label
                          htmlFor="legalGuardianCheckbox"
                          className={styles.labelfield}
                        >
                          Legal Guardian
                        </label>
                      </p>
                      <img src={User} alt="" />
                    </div>
                  </FormGroup>
                </Col>
                {
                  <div
                    className={cx(
                      { [styles.fadein]: isLegalGuardian },
                      { [styles.fadeout]: !isLegalGuardian }
                    )}
                    style={{
                      visibility: isLegalGuardian ? "visible" : "hidden",
                    }}
                  >
                    <Col sm={12}>
                      <FormGroupInput
                        id="inputFieldLegalGuardianName"
                        displayText="Guardian's name"
                        style={styles.customInput}
                        handleChange={setGuardianName}
                        value={guardianName}
                        disable={isDisabled}
                        validate={isValidName}
                      />
                    </Col>
                    <Label className={styles.customformLabel}>Address:</Label>
                    <Row className="ml-0 mr-0">
                      <Col sm={6}>
                        <GuardianSearchLocationInput
                          updateAddress={updateAddress}
                          value={guardianAddress && guardianAddress.street}
                          user={""}
                        />
                      </Col>
                      <Col sm={6}>
                        <FormGroupInput
                          id="inputFieldLegalGuardianStreetNumber"
                          displayText="Street No."
                          value={guardianAddress && guardianAddress.streetNo}
                          style={styles.customInput}
                          handleChange={(value) =>
                            updateAddress(value, "", "streetNo")
                          }
                          disable={isDisabled}
                          validate={isAlphaNumStr}
                        />
                      </Col>
                      <Col sm={6}>
                        <FormGroupInput
                          id="inputFieldLegalGuardianPostalCode"
                          displayText="Postal Code"
                          value={guardianAddress && guardianAddress.postalCode}
                          style={styles.customInput}
                          handleChange={(value) =>
                            updateAddress(value, "", "postalCode")
                          }
                          disable={isDisabled}
                          validate={isValidPostalCode}
                        />
                      </Col>
                      <Col sm={6}>
                        <FormGroupInput
                          id="inputFieldLegalGuardianCity"
                          displayText="City"
                          value={guardianAddress && guardianAddress.city}
                          style={styles.customInput}
                          handleChange={(value) =>
                            updateAddress(value, "", "city")
                          }
                          disable={isDisabled}
                          validate={isAlphaNumStr}
                        />
                      </Col>
                      <Col sm={12}>
                        <FormGroupInput
                          id="inputFieldLegalGuardianCountry"
                          displayText="Country"
                          value={guardianAddress && guardianAddress.country}
                          style={styles.customInput}
                          handleChange={(value) =>
                            updateAddress(value, "", "country")
                          }
                          disable={isDisabled}
                          validate={isAlphaNumStr}
                        />
                      </Col>
                    </Row>
                    <Col sm={12}>
                      <FormGroupInput
                        id="inputFieldLegalGuardianPhone"
                        displayText="Guardian's phone number"
                        style={styles.customInput}
                        handleChange={setGuardianPhone}
                        value={guardianPhone}
                        disable={isDisabled}
                        validate={isValidPhoneNumber}
                      />
                    </Col>

                    <Col sm={12}>
                      <FormGroupInput
                        id="inputFieldLegalGuardianEmail"
                        displayText="Guardian's email"
                        style={styles.customInput}
                        handleChange={setGuardianEmail}
                        value={guardianEmail}
                        disable={isDisabled}
                        validate={isValidEmailAddress}
                      />
                    </Col>
                  </div>
                }
              </Row>
              {!isDisabled && (
                <div className={styles.Modalfooter}>
                  <Button
                    color="primary"
                    onClick={handleSubmit}
                    className={styles.primaryBtn}
                  >
                    <img src={whitecircletick} alt="" />
                    Create
                  </Button>{" "}
                  <Button
                    color="secondary"
                    onClick={handleClose}
                    className={styles.cancelBtn}
                  >
                    Cancel
                  </Button>
                </div>
              )}
            </Form>
          </div>
        </ModalBody>
      </Modal>
      <Notification
        modal={notificationModal}
        setModal={setNotificationModal}
        status={requestStatus}
      />
    </div>
  );
};

UpdateUserInfo.propTypes = {
  modal: PropTypes.bool,
  toggle: PropTypes.func,
  nestedModal: PropTypes.bool,
  nestedToggle: PropTypes.func,
  setNestedModal: PropTypes.func,
  patient: PropTypes.object,
};

export default UpdateUserInfo;
