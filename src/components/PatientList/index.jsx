import React, { Fragment, useState } from "react";
import PropTypes from "prop-types";
import { Table } from "reactstrap";

import styles from "./patientList.module.scss";
import UpdateRole from "../UpdateStatus";
import ToggleMenu from "../ToggleMenu";
import formatDate from "../../utils/formatDate";
import getTimeDifference from "../../utils/getTimeDifference";
import PatientService from "../../api/patient";
import UpdateUserInfo from "../UpdateUserInfo";
import { STATUS } from "../../constants";
import Defaultimage from "../DefaultImage";

const edit =
  "https://storage.googleapis.com/dechea-microservices-files/patient-management/edit.svg";
const bin =
  "https://storage.googleapis.com/dechea-microservices-files/patient-management/bin.svg";
const sortUp =
  "https://storage.googleapis.com/dechea-microservices-files/patient-management/sortUp.svg";
const sortDown =
  "https://storage.googleapis.com/dechea-microservices-files/patient-management/sortDown.svg";

const HiddenImage = ({ sortAssending }) => (
  <img
    className={styles.hidden}
    src={!sortAssending ? sortDown : sortUp}
    alt="arrow"
  ></img>
);

const PatientList = ({ patients, forceUpdate, activeTab }) => {
  const [modal, setModal] = useState(false);
  const [nestedModal, setNestedModal] = useState(false);
  const [editPatient, setEditPatient] = useState({});
  const [disableFields, setDisableFields] = useState(true);
  const [lastSelected, setLastSelected] = useState("");
  const [highlightColumn, setHighlightColumn] = useState("column-4");
  const [sortAssending, setSortAssending] = useState(false);

  const toggle = () => setModal(!modal);
  const nestedToggle = () => setNestedModal(!nestedModal);

  const getFormattedStatus = (inputStatus) => {
    const result = STATUS.filter((status) => status.value === inputStatus);
    if (result.length) {
      return { value: result[0].value, label: result[0].label };
    }
    return { value: "NOT_PRESENT", label: "Not Present" };
  };

  const getToggleItems = (patient) => [
    {
      label: "Edit info",
      handleClick: () => {
        setModal(true);
        setDisableFields(false);
        setEditPatient(patient);
      },
      logo: edit,
    },
    {
      label: "Archive",
      handleClick: () => archivePatient(patient),
      logo: bin,
    },
  ];

  const archivePatient = async (patient) => {
    await PatientService.archivePatient(patient.patients[0].id);
    forceUpdate();
  };

  const showDetails = (patient) => {
    setEditPatient(patient);
    setModal(true);
  };

  const sortData = (key, order) => {
    return function innerSort(a, b) {
      let variableA, variableB;
      if (key === "addresses") {
        variableA =
          typeof a.addresses[0].street === "string"
            ? a.addresses[0].street.toUpperCase()
            : a[key];
        variableB =
          typeof b.addresses[0].street === "string"
            ? b.addresses[0].street.toUpperCase()
            : b[key];
      } else if (key === "status") {
        variableA =
          typeof a.patients[0].status === "string"
            ? a.patients[0].status.toUpperCase()
            : a[key];
        variableB =
          typeof b.patients[0].status === "string"
            ? b.patients[0].status.toUpperCase()
            : b[key];
      } else {
        variableA = typeof a[key] === "string" ? a[key].toUpperCase() : a[key];
        variableB = typeof b[key] === "string" ? b[key].toUpperCase() : b[key];
      }
      let comparison = 0;
      if (variableA > variableB) {
        comparison = 1;
      } else if (variableA < variableB) {
        comparison = -1;
      }
      return order ? comparison * -1 : comparison;
    };
  };

  const handleClick = (param) => {
    if (lastSelected === param) {
      patients.sort(sortData(param, !sortAssending));
      setSortAssending(!sortAssending);
    } else {
      setSortAssending(false);
      patients.sort(sortData(param, false));
      setLastSelected(param);
    }
  };

  return (
    <div className={styles.rootWrapper}>
      <Table id="patient_table">
        <thead>
          <tr>
            {!!(activeTab !== "4") && <th> </th>}
            <th
              className={highlightColumn === "column-1" && styles.headerFont}
              onClick={() => {
                setHighlightColumn("column-1");
                handleClick("firstName");
              }}
            >
              Patient
              {highlightColumn === "column-1" ? (
                <img
                  className={styles.arrowImage}
                  src={!sortAssending ? sortDown : sortUp}
                  alt="arrow"
                ></img>
              ) : (
                <HiddenImage sortAssending={sortAssending} />
              )}
            </th>
            <th
              className={highlightColumn === "column-2" && styles.headerFont}
              onClick={() => {
                setHighlightColumn("column-2");
                handleClick("addresses");
              }}
            >
              Address
              {highlightColumn === "column-2" ? (
                <img
                  className={styles.arrowImage}
                  src={!sortAssending ? sortDown : sortUp}
                  alt="arrow"
                ></img>
              ) : (
                <HiddenImage sortAssending={sortAssending} />
              )}
            </th>
            <th
              className={highlightColumn === "column-3" && styles.headerFont}
              onClick={() => {
                setHighlightColumn("column-3");
                handleClick("birthdate");
              }}
            >
              Date of Birth
              {highlightColumn === "column-3" ? (
                <img
                  className={styles.arrowImage}
                  src={!sortAssending ? sortDown : sortUp}
                  alt="arrow"
                ></img>
              ) : (
                <HiddenImage sortAssending={sortAssending} />
              )}
            </th>
            {activeTab !== "3" ? (
              <th
                className={
                  highlightColumn === "column-4"
                    ? styles.appointmentHeader
                    : styles.minWidth
                }
                onClick={() => {
                  setHighlightColumn("column-4");
                  handleClick("lastAppointment");
                }}
              >
                Appointment date
                {highlightColumn === "column-4" ? (
                  <img
                    className={styles.arrowImage}
                    src={!sortAssending ? sortDown : sortUp}
                    alt="arrow"
                  ></img>
                ) : (
                  <HiddenImage sortAssending={sortAssending} />
                )}
              </th>
            ) : (
              <></>
            )}
            <th
              className={highlightColumn === "column-5" && styles.headerFont}
              onClick={() => {
                setHighlightColumn("column-5");
                handleClick("status");
              }}
            >
              Status
              {highlightColumn === "column-5" ? (
                <img
                  className={styles.arrowImage}
                  src={!sortAssending ? sortDown : sortUp}
                  alt="arrow"
                ></img>
              ) : (
                <HiddenImage sortAssending={sortAssending} />
              )}
            </th>
            <th />
          </tr>
        </thead>
        <tbody>
          {patients.map((patient) => (
            <Fragment key={patient.id}>
              <tr>
                {(!patient.archived ||
                  (patient.patients && patient.patients.length) ||
                  !(
                    patient.patients &&
                    patient.patients.length &&
                    patient.patients[0].archived
                  )) &&
                  activeTab !== "4" && (
                    <img
                      src="https://storage.googleapis.com/dechea-microservices-files/patient-management/bellIcon.svg"
                      alt="bell"
                      className={styles.bellIcon}
                    />
                  )}
                <td
                  onClick={() => showDetails(patient)}
                  className={styles.tableRow}
                >
                  <div
                    className={
                      highlightColumn === "column-1"
                        ? styles.highlightNameWrapper
                        : styles.nameWrapper
                    }
                  >
                    <div
                      className={
                        styles[
                          `circle${
                            patient &&
                            patient.patients &&
                            patient.patients.length &&
                            (patient.patients[0].status === "WAITROOM"
                              ? "Yellow"
                              : patient.patients[0].status === "TREATMENT"
                              ? "Green"
                              : patient.patients[0].status === "NOTIFIED_DELAY"
                              ? "Orange"
                              : patient.patients[0].status === "NOT_ARRIVED"
                              ? "Blue"
                              : "None")
                          }`
                        ]
                      }
                    >
                      {patient.profileImage ? (
                        <img
                          className={styles.profileImage}
                          src="https://i.stack.imgur.com/l60Hf.png"
                          alt="profile"
                        />
                      ) : (
                        <Defaultimage className={styles.defImage} />
                      )}
                    </div>
                    <div className={styles.ageWrapper}>
                      {`${patient.firstName} ${patient.lastName}`}
                      <div className={styles.age}>
                        {Math.floor(
                          (new Date() - new Date(patient.birthdate)) /
                            3600000 /
                            24 /
                            365
                        )}{" "}
                        years
                      </div>
                    </div>
                  </div>
                </td>
                <td
                  onClick={() => showDetails(patient)}
                  className={
                    highlightColumn === "column-2"
                      ? styles.highlightAddress
                      : styles.address
                  }
                >
                  <div>{`${patient.addresses[0]?.street || ""}, ${
                    patient.addresses[0]?.streetNo || ""
                  },`}</div>
                  {`${patient.addresses[0]?.postalCode || ""} ${
                    patient.addresses[0]?.city || ""
                  } ${patient.addresses[0]?.country || ""}`}
                </td>
                <td
                  onClick={() => showDetails(patient)}
                  className={
                    highlightColumn === "column-3"
                      ? styles.highlightDob
                      : styles.dob
                  }
                >
                  {formatDate(patient.birthdate)}
                </td>
                {activeTab !== "3" ? (
                  <td
                    onClick={() => showDetails(patient)}
                    className={
                      highlightColumn === "column-4"
                        ? styles.highlightAppointmentContent
                        : styles.appointmentContent
                    }
                  >
                    {patient &&
                    patient.patients &&
                    patient.patients.length &&
                    patient.patients[0].lastAppointment
                      ? getTimeDifference(
                          new Date(),
                          new Date(patient.patients[0].lastAppointment)
                        )
                      : "No Appointment"}
                  </td>
                ) : (
                  <></>
                )}
                <td className={styles.statusRow}>
                  <UpdateRole
                    role={getFormattedStatus(
                      patient &&
                        patient.patients &&
                        patient.patients.length &&
                        patient.patients[0].status
                    )}
                    forceUpdate={forceUpdate}
                    people={patient}
                  />
                </td>
                {!(
                  patient.patients &&
                  patient.patients.length &&
                  patient.patients[0] &&
                  patient.patients[0].archived
                ) && (
                  <td className={styles.archiveRow}>
                    <ToggleMenu items={getToggleItems(patient)} />
                  </td>
                )}
              </tr>
            </Fragment>
          ))}
        </tbody>
      </Table>
      <UpdateUserInfo
        modalId="userModal"
        toggle={toggle}
        modal={modal}
        nestedModal={nestedModal}
        setNestedModal={setNestedModal}
        nestedToggle={nestedToggle}
        patient={editPatient}
        isDisabled={disableFields}
        setDisableFields={() => setDisableFields(true)}
        forceUpdate={forceUpdate}
      />
    </div>
  );
};

PatientList.propTypes = {
  patients: PropTypes.array,
};

export default PatientList;
