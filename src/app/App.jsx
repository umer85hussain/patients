import React from "react";

import Routes from "../Routes";

function App() {
  if (window && window.dataLayer) {
    window.dataLayer.push({
      event: "pageview",
    });
  }
  return <Routes />;
}

export default App;
