import React from "react";
import { FormGroup, Input, FormFeedback } from "reactstrap";
import styles from "./formGroupInput.module.scss";

const FormGroupInput = ({
  id,
  displayText,
  style,
  handleChange,
  value,
  disable,
  autoCompleteRef,
  validate,
  requiredError,
}) => {
  const valid =
    validate && value && value.length
      ? validate(value)
        ? false
        : true
      : false;

  return (
    <FormGroup>
      <Input
        id={id}
        type="text"
        innerRef={autoCompleteRef}
        placeholder={displayText}
        className={style}
        autoComplete="off"
        onChange={(event) => handleChange(event.target.value)}
        value={value}
        disabled={disable}
        invalid={valid || requiredError}
      />
      {!!requiredError && (
        <FormFeedback
          className={
            requiredError ? styles.feedbackForm : styles.feedbackFormNone
          }
        >
          {Object.values(requiredError)[0]}
        </FormFeedback>
      )}
      {!requiredError && (
        <FormFeedback
          className={
            value !== undefined &&
            value !== null &&
            (valid ? styles.feedbackForm : styles.feedbackFormNone)
          }
        >
          *invalid input
        </FormFeedback>
      )}
    </FormGroup>
  );
};

export default FormGroupInput;
