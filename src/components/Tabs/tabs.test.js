import React from "react";
import { render } from "@testing-library/react";
import CustomNavItem from "./NavItem";
import TabPanel from "./TabPanel";

it("test nav item text", () => {
  const { getByText } = render(
    <CustomNavItem activeTab={"1"} navIndex="1" title="All Patients" />
  );
  getByText("All Patients");
});

it("test tab panel text", () => {
  const { getByText } = render(
    <TabPanel navIndex="1">
      <div>Test text</div>
    </TabPanel>
  );
  getByText("Test text");
});
