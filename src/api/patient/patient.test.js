import PatientService from "./";
import mockList from "../../components/PatientList/mocks";

const getPatientMock = jest.spyOn(PatientService, "getPatient");
getPatientMock.mockImplementation(() => mockList);

it("should return patient", async () => {
  try {
    const res = await getPatientMock();
    expect(res).toEqual(mockList);
  } catch (e) {
    throw new Error(e.message);
  }
});

const getPatientsMock = jest.spyOn(PatientService, "getPatients");
getPatientsMock.mockImplementation((number) => mockList[number]);

it("should return patients", async () => {
  try {
    const res = await getPatientsMock(1);
    expect(res).toEqual(mockList[1]);
  } catch (e) {
    throw new Error(e.message);
  }
});

const addPatientMock = jest.spyOn(PatientService, "addPatient");
addPatientMock.mockImplementation((patient) => ({ statusCode: 201 }));

it("should add patient", async () => {
  try {
    const res = await addPatientMock(mockList[0]);
    expect(res.statusCode).toEqual(201);
  } catch (e) {
    throw new Error(e.message);
  }
});

const archivePatientMock = jest.spyOn(PatientService, "archivePatient");
archivePatientMock.mockImplementation((id) => ({ statusCode: 204 }));

it("should archive patient", async () => {
  try {
    const res = await archivePatientMock(1);
    expect(res.statusCode).toEqual(204);
  } catch (e) {
    throw new Error(e.message);
  }
});

const updatePatientMock = jest.spyOn(PatientService, "updatePatient");
updatePatientMock.mockImplementation((patient) => ({
  ...patient,
  statusCode: 201,
}));

it("should update patient details", async () => {
  try {
    const res = await updatePatientMock(mockList[1]);
    expect(res).toEqual({ ...mockList[1], statusCode: 201 });
  } catch (e) {
    throw new Error(e.message);
  }
});

const updatePatientStatusMock = jest.spyOn(
  PatientService,
  "updatePatientStatus"
);
updatePatientStatusMock.mockImplementation((id, status) => ({
  status,
  statusCode: 201,
}));

it("should update patient status", async () => {
  try {
    const res = await updatePatientStatusMock(1, "NOT_PRESENT");
    expect(res).toEqual({ status: "NOT_PRESENT", statusCode: 201 });
  } catch (e) {
    throw new Error(e.message);
  }
});
