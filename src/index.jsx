import React from "react";
import ReactDOM from "react-dom";
import TagManager from "react-gtm-module";
import "bootstrap/dist/css/bootstrap.min.css";

import App from "./app/App";
import "./index.css";

const tagManagerArgs = {
  gtmId: "GTM-T3DK9F8",
};

TagManager.initialize(tagManagerArgs);

window.renderPatients = (containerId, history) => {
  console.log("Patient Management renderPatients from microservice 1");
  ReactDOM.render(
    <App history={history} />,
    document.getElementById(containerId)
  );
};

window.unmountPatients = (containerId) => {
  console.log("Patient Management unmountPatients from microservice");
  const node = document.getElementById(containerId);
  ReactDOM.unmountComponentAtNode(node);
};
