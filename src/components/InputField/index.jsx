import React from "react";
import cx from "classnames";

import styles from "./input.module.scss";

const InputField = ({
  handleChange,
  label,
  labelClass,
  inputClass,
  value,
  isDisabled,
}) => {
  return (
    <fieldset className={styles["form-field"]}>
      <input
        type="text"
        className={cx(styles["form-input"], inputClass)}
        onChange={handleChange}
        value={value}
        disabled={isDisabled}
      />
      <label className={cx(styles["form-label"], labelClass)}>{label}</label>
    </fieldset>
  );
};

export default InputField;
