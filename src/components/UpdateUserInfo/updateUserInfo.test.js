import React from "react";
import { render, fireEvent } from "@testing-library/react";
import UpdateUserInfo from "./index";
import { mockGoogle } from "../../utils/mockGoogle";

window.google = global.google = mockGoogle();

const patient = [
  {
    firstName: "Franky",
    lastName: "Stiller",
    title: "Prof.",
    birthdate: "2000-01-23",
    phone: "+49 831 54091090",
    patientSince: "2000-01-23",

    job: "Manager",
    lastAppointment: "2000-01-23",

    email: "franky.stiller@test.com",

    legalGuardians: [],
    patients: [
      {
        insuranceNumber: "BZT301381240",
        patientSince: "",
        lastAppointment: null,
        status: "WAITING",
        careLevel: 5,
        familyDoctor: "Mark Sinzo",
        archived: false,
        notes: [],
        images: [],
        insurance: [{ id: 6, type: "LEGAL", archived: false }],
        ward: false,
      },
    ],
    addresses: [
      {
        street: "Kottenerstr.",
        streetNo: "80",
        postalCode: "87435",
        city: "Kempten",
        country: "Germany",
      },
    ],
  },
];

it("test update patient info", () => {
  const { container } = render(<UpdateUserInfo patient={patient[0]} />);
  expect(container.firstChild).toHaveClass("modalSection");
});

it("test update patient info", () => {
  const { getByText } = render(
    <UpdateUserInfo patient={patient[0]} modal={true} />
  );
  getByText("Upload photo");
});
