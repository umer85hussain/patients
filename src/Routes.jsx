import React from "react";
import { Route, Router, Switch, Redirect } from "react-router-dom";

import { history } from "./app/history";
import Dashboard from "./pages/dashboard";

const Routes = () => {
  return (
    <Router history={history}>
      <Switch>
        <Route history={history} component={Dashboard} path="/" />
        <Redirect from="*" to="/" />
      </Switch>
    </Router>
  );
}

export default Routes;
