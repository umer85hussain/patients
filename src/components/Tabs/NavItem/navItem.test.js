import React from "react";
import { render, fireEvent } from "@testing-library/react";
import NavItem from "./";

it("test tab panel text function call", () => {
  const { getByText } = render(
    <NavItem navIndex="1" title="test" toggle={() => { }} />
  );
  const link = getByText("test")
  fireEvent.click(link)
});
