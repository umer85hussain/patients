const { Selector } = require("testcafe");
const { Given, When, Then } = require('@cucumber/cucumber');
const patientmanagementPage = require('../../support/pages/patient-management')

Then("Carmen sees all patients", async function() {
  await testController.wait(1000)
      .expect(patientmanagementPage.patientmanagement.patientTable().count)
      .gt(0);
});