Feature: Administrate all my patients

  Scenario: Dentist assistant adds new patient
    Given Carmen is logged in
    When Carmen adds a new patient with valid data
    And Carmen saves the patient
    Then Carmen sees the new patient in the overview

  Scenario: Dentist assistant adds new patient with legal guardian
    Given Carmen is logged in
    When Carmen adds a new patient with valid data
    And Carmen adds a new legal guardian to the patient with valid data
    And Carmen saves the patient
    Then Carmen sees the new patient in the overview