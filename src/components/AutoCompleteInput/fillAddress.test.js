import fillAddress from "./fillAddress";

const testAddress = [
  { long_name: "342", short_name: "342", types: ["street_number"] },
  {
    long_name: "Luxemburger Straße",
    short_name: "Luxemburger Str.",
    types: ["route"],
  },
  {
    long_name: "Lindenthal",
    short_name: "Lindenthal",
    types: ["sublocality_level_1", "sublocality", "political"],
  },
  { long_name: "Köln", short_name: "Köln", types: ["locality", "political"] },
  {
    long_name: "Köln",
    short_name: "K",
    types: ["administrative_area_level_2", "political"],
  },
  {
    long_name: "Nordrhein-Westfalen",
    short_name: "NRW",
    types: ["administrative_area_level_1", "political"],
  },
  { long_name: "Germany", short_name: "DE", types: ["country", "political"] },
  { long_name: "50937", short_name: "50937", types: ["postal_code"] },
];

const expectedOutput = {
  street: "Lindenthal, Luxemburger Straße",
  streetNo: "342",
  city: "Köln, Nordrhein-Westfalen",
  state: "Nordrhein-Westfalen",
  country: "Germany",
  postalCode: "50937",
};

it("test fillAddress function", () => {
  const addressMock = jest.fn((testAddress) => fillAddress(testAddress));
  addressMock(testAddress);
  expect(addressMock.mock.calls.length).toBe(1);
  expect(addressMock.mock.results[0].value).toStrictEqual(expectedOutput);
});
