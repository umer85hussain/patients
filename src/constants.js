export const BASE_API_URL =
  "https://be-patient-management-rq234cdqga-ew.a.run.app/core/patient-management/api/v1";

export const STATUS = [
  { value: "WAITROOM", label: "Waitroom", color: "#F9CE60" },
  { value: "NOT_PRESENT", label: "Not Present", color: "#7E98BA" },
  { value: "TREATMENT", label: "Treatment", color: "#36DCAA" },
  { value: "NOTIFIED_DELAY", label: "Notified delay", color: "#ED5611" },
  { value: "NOT_ARRIVED", label: "Not arrived", color: "#0A7291" },
];

export const TITLE_OPTIONS = [
  { value: "Male", label: "Male" },
  { value: "Female", label: "Female" },
  { value: "Diverse", label: "Diverse" },
];

export const DEGREE_OF_CARE_OPTIONS = [
  { value: 0, label: "0" },
  { value: 1, label: "1" },
  { value: 2, label: "2" },
  { value: 3, label: "3" },
  { value: 4, label: "4" },
  { value: 5, label: "5" },
];

export const MONTH_OPTIONS = [
  { value: 1, label: "January" },
  { value: 2, label: "February" },
  { value: 3, label: "March" },
  { value: 4, label: "April" },
  { value: 5, label: "May" },
  { value: 6, label: "June" },
  { value: 7, label: "July" },
  { value: 8, label: "August" },
  { value: 9, label: "September" },
  { value: 10, label: "October" },
  { value: 11, label: "November" },
  { value: 12, label: "December" },
];

export const SELECT_ROLE_STYLE = {
  control: (base, state) => {
    let borderColor = "#E8ECF2";
    let backgroundColor = "none";
    const result = state.getValue();
    if (result.length) {
      borderColor =
        result[0].label === "Waitroom"
          ? "#F2E24E"
          : result[0].label === "Treatment"
          ? "#36DCAA"
          : result[0].label === "Notified delay"
          ? "#ED5611"
          : result[0].label === "Not arrived"
          ? "#0A7291"
          : "#B6C0D1";
      backgroundColor =
        result[0].label === "Waitroom"
          ? "#FFFDEC"
          : result[0].label === "Treatment"
          ? "#F7FFFC"
          : result[0].label === "Notified delay"
          ? "#FEF2EC"
          : result[0].label === "Not arrived"
          ? "#ECFAFE"
          : "#FFFFFF";
    }
    return {
      ...base,
      borderRadius: 12,
      fontFamily: "Montserrat, sans-serif",
      fontStyle: "normal",
      fontWeight: 500,
      fontSize: "13px",
      lineHeight: "18px",
      color: "#263857",
      cursor: "pointer",
      width: "200px",
      border: `1px solid ${borderColor}`,
      boxShadow: "none",
      backgroundColor: backgroundColor,
      "&:hover": { borderColor: borderColor },
      paddingTop: "6px",
      paddingBottom: "6px",
      paddingLeft: "24px",
    };
  },
  menu: (base, state) => ({
    ...base,
    borderRadius: 12,
    hyphens: "auto",
    marginTop: 0,
    textAlign: "left",
    wordWrap: "break-word",
    color: "#DAE2EF",
    width: "200px",
  }),
  menuList: (base) => ({
    ...base,
    padding: 0,
    background: "#263857",
    borderRadius: 12,
  }),
  option: (styles, { isFocused }) => {
    return {
      ...styles,
      background: isFocused ? "#314363" : "#263857",
      boxShadow:
        "0px 25px 70px rgba(53, 69, 98, 0.15), 0px 20px 50px rgba(53, 69, 98, 0.15)",
      backdropFilter: "blur(144px)",
      borderBottom: "1px solid #3c4e6f",
      borderRadius: 12,
      "&:hover": {
        backgroundColor: "#314363",
        color: "#DAE2EF",
      },
    };
  },
  valueContainer: () => ({
    padding: "0px",
    margin: "0px",
    lineHeight: "13px",
  }),
  singleValue: (base, { isDisabled }) => ({
    color: isDisabled ? "#263857" : "#263857",
    fontWeight: 700,
    marginLeft: "2px",
    marginRight: "2px",
    maxWidth: "calc(100% - 8px)",
    position: "absolute",
    textOverflow: "ellipsis",
    whiteSpace: "nowrap",
    top: "50%",
    transform: "translateY(-50%)",
    boxSizing: "border-box",
    overflow: "unset",
  }),
  dropdownIndicator: (base) => ({
    ...base,
    color: "black",
    marginRight: "10px",
    width: "30px",
  }),
};

export const emptyPatient = {
  gender: null,
  firstName: null,
  lastName: null,
  email: null,
  phone: null,
  profession: null,
  birthdate: null,
  archived: false,
  legalGuardians: [
    {
      firstName: null,
      lastName: null,
      email: null,
      phone: null,
      archived: false,
      legalGuardians: [],
      patients: [],
      addresses: [
        {
          street: null,
          streetNo: null,
          postalCode: null,
          city: null,
          country: null,
          archived: false,
        },
      ],
    },
  ],
  patients: [
    {
      insuranceNumber: null,
      patientSince: null,
      lastAppointment: null,
      status: null,
      careLevel: null,
      familyDoctor: null,
      archived: false,
      notes: [],
      images: [],
      insurance: {
        id: 1,
        type: null,
        archived: false,
      },
      ward: false,
    },
  ],
  addresses: [
    {
      street: null,
      streetNo: null,
      postalCode: null,
      city: null,
      country: null,
      archived: false,
    },
  ],
};

export const SELECT_ADDRESS_STYLE = {
  control: (base) => {
    let borderColor = "#E8ECF2";
    let backgroundColor = "none";
    return {
      ...base,
      borderRadius: 0,
      fontFamily: "Montserrat, sans-serif",
      fontStyle: "normal",
      fontWeight: 500,
      fontSize: "13px",
      lineHeight: "18px",
      color: "#263857",
      cursor: "pointer",
      width: "100%",
      border: `0px solid ${borderColor}`,
      borderBottom: `1px solid ${borderColor}`,
      boxShadow: "none",
      backgroundColor: backgroundColor,
      paddingTop: "16px",
      paddingBottom: "16px",
      paddingLeft: "24px",
    };
  },
  menu: (base) => ({
    ...base,
    borderRadius: "12px",
    hyphens: "auto",
    marginTop: 0,
    textAlign: "left",
    wordWrap: "break-word",
    color: "#DAE2EF",
    width: "100%",
  }),
  menuList: (base) => ({
    ...base,
    padding: 0,
    background: "#263857",
    borderRadius: "12px",
  }),
  option: (styles, state) => {
    return {
      ...styles,
      background: state.isFocused ? "#314363" : "#263857",
      boxShadow:
        "0px 25px 70px rgba(53, 69, 98, 0.15), 0px 20px 50px rgba(53, 69, 98, 0.15)",
      backdropFilter: "blur(144px)",
      borderRadius: "12px",
      borderBottom: "1px solid #3c4e6f",
      "&:hover": {
        backgroundColor: "#314363",
        color: "#DAE2EF",
      },
    };
  },
  valueContainer: () => ({
    padding: "0px",
    margin: "0px",
    lineHeight: "13px",
  }),
  singleValue: (base, { isDisabled }) => ({
    ...base,
    color: isDisabled ? "#263857" : "#263857",
  }),
};

export const SELECT_TITLE_STYLE = {
  control: (base) => {
    let borderColor = "#E8ECF2";
    let backgroundColor = "none";
    return {
      ...base,
      borderRadius: 0,
      marginTop: "10px",
      fontFamily: "Montserrat, sans-serif",
      fontStyle: "normal",
      fontWeight: 500,
      fontSize: "18px",
      lineHeight: "18px",
      color: "#263857",
      cursor: "pointer",
      width: "100%",
      border: `0px solid ${borderColor}`,
      borderBottom: `1px solid ${borderColor}`,
      boxShadow: "none",
      backgroundColor: backgroundColor,
      paddingBottom: "5px",
      overFlow: "none !important",
    };
  },
  placeholder: (defaultStyles) => {
    return {
      ...defaultStyles,
      color: "#7E98BA",
    };
  },
  menu: (base) => ({
    ...base,
    borderRadius: "12px",
    hyphens: "auto",
    marginTop: 0,
    textAlign: "left",
    wordWrap: "break-word",
    color: "#DAE2EF",
    width: "100%",
  }),
  menuList: (base) => ({
    ...base,
    padding: 0,
    background: "#263857",
    borderRadius: "12px",
  }),
  option: (styles, state) => {
    return {
      ...styles,
      background: state.isFocused ? "#314363" : "#263857",
      boxShadow:
        "0px 25px 70px rgba(53, 69, 98, 0.15), 0px 20px 50px rgba(53, 69, 98, 0.15)",
      backdropFilter: "blur(144px)",
      borderRadius: "12px",
      borderBottom: "1px solid #3c4e6f",
      "&:hover": {
        backgroundColor: "#314363",
        color: "#DAE2EF",
      },
    };
  },
  valueContainer: () => ({
    padding: "0px",
    margin: "0px",
    lineHeight: "22px",
  }),
  singleValue: (base, { isDisabled }) => ({
    ...base,
    color: isDisabled ? "#263857" : "#263857",
  }),
};


export const GOOGLE_AUTOCOMPLETE_API_KEY = "AIzaSyBmubS5K0MEoYL2H69zzUyqcGA3mQr2CFU";
