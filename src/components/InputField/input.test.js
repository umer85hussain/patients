import React from "react";
import { render } from "@testing-library/react";
import Input from "./index";

it("test input field changes", () => {
  const { getByText } = render(
    <Input label={"Hello"} />
  );
  getByText("Hello");
});
