import React, { useState } from "react";
import { TabContent, Nav } from "reactstrap";

import styles from "./tabs.module.scss";
import CustomNavItem from "./NavItem";
import TabPanel from "./TabPanel";
import PatientList from "../PatientList";
import NoPatient from "../../pages/NoPatient";
import categorisePatients from "../../utils/categorisePatients";

const PatientTabs = ({ state, forceUpdate }) => {

  const [activeTab, setActiveTab] = useState("1");

  const {
    todaysPatients,
    noAppointmentPatients,
    archivedPatients,
    allPatients,
  } = categorisePatients(state.patients);

  const toggle = (tab) => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  return (
    <>
      {state && state.patients && state.patients.length ? (
        <div className={styles.rootWrapper}>
          <Nav tabs className={styles["nav-tabs"]}>
            <CustomNavItem
              activeTab={activeTab}
              navIndex="1"
              toggle={toggle}
              title="All Patients"
            />
            <CustomNavItem
              activeTab={activeTab}
              navIndex="2"
              toggle={toggle}
              title="Today’s Patients"
            />
            <CustomNavItem
              activeTab={activeTab}
              navIndex="3"
              toggle={toggle}
              title="With no appointments"
            />
            <CustomNavItem
              activeTab={activeTab}
              navIndex="4"
              toggle={toggle}
              title={`Archived (${archivedPatients.length})`}
            />
          </Nav>
          <TabContent className={styles.contentWrapper} activeTab={activeTab}>
            <TabPanel navIndex="1">
              <PatientList
                forceUpdate={forceUpdate}
                patients={allPatients}
                activeTab={activeTab}
              />
            </TabPanel>
            <TabPanel navIndex="2">
              <PatientList
                forceUpdate={forceUpdate}
                patients={todaysPatients}
                activeTab={activeTab}
              />
            </TabPanel>
            <TabPanel navIndex="3">
              <PatientList
                forceUpdate={forceUpdate}
                patients={noAppointmentPatients}
                activeTab={activeTab}
              />
            </TabPanel>
            <TabPanel navIndex="4">
              <PatientList
                forceUpdate={forceUpdate}
                patients={archivedPatients}
                activeTab={activeTab}
              />
            </TabPanel>
          </TabContent>
        </div>
      ) : (
          <NoPatient forceUpdate={forceUpdate} />
        )
      }
    </>
  );
};

export default PatientTabs;
