import React, { useEffect } from "react";
import { Modal, ModalBody } from "reactstrap";
import styles from "./notification.module.scss";
import cx from "classnames";

import OutsideAlerter from "../OutsideClickHandler";

const tickmark = "https://storage.googleapis.com/dechea-microservices-files/patient-management/tickmark.svg"
const crossIcon = "https://storage.googleapis.com/dechea-microservices-files/patient-management/crossIcon.svg"

const Notification = ({ modal, setModal, status }) => {

  useEffect(() => {
    const interval = setTimeout(() => {
      setModal(false)
    }, 2000);
    return () => clearInterval(interval);
  }, [modal, setModal]);

  return (
    <OutsideAlerter actionHandler={() => setModal(false)}>
      <Modal
        isOpen={modal}
        backdrop={false}
        fade={false}
        className={styles.modal}
        contentClassName={cx({
          [styles.contentSuccess]: status === "Success",
          [styles.contentError]: status === "Error"
        })}
      >
        <ModalBody
          className={cx({
            [styles.modalBody]: status === "Success",
            [styles.modalBodyError]: status === "Error"
          })}
        >
          <div
            className={cx({
              [styles.buttonWrapper]: status === "Success",
              [styles.buttonWrapperError]: status === "Error"
            })}
          >
            <div className={styles.plusWrapper}>
              <img
                className={cx({
                  [styles.plus]: status === "Success",
                  [styles.cross]: status === "Error"
                })}
                src={status === "Success" ? tickmark : crossIcon}
                alt="success"
              />
            </div>
            <div>
              <div
                className={cx({
                  [styles.messageTitle]: status === "Success",
                  [styles.messageTitleError]: status === "Error"
                })}>
                {status}
              </div>
              <div className={styles.messageText}>Message</div>
            </div>
          </div>
        </ModalBody>
      </Modal>
    </OutsideAlerter>
  );
};

export default Notification;
