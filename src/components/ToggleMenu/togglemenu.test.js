import React from "react";
import { render } from "@testing-library/react";
import styles from "./toggleMenu.module.scss";

const edit = "https://storage.googleapis.com/dechea-microservices-files/patient-management/edit.svg"
const bin = "https://storage.googleapis.com/dechea-microservices-files/patient-management/bin.svg"

const items = [
  {
    label: "Edit info",
    handleClick: () => false,
    logo: edit,
  },
  {
    label: "Archive",
    handleClick: () => false,
    logo: bin,
  },
]

it("test togglemenu text", () => {
  const { getByText } = render(
    <>
      {items.map((item) => (
        <div className={styles.item}>
          <img src={item.logo} alt="icon" />
          <div className={styles.label} >{item.label}</div>
        </div>
      ))}
    </>
  );
  getByText("Edit info");
  getByText("Archive");
});

