import React from "react";
import { Modal, ModalBody, Button } from "reactstrap";

import styles from "./nestedModal.module.scss";

const NestedModal = ({ nestedModal, nestedToggle, toggleAll }) => {
  return (
    <Modal isOpen={nestedModal} className={styles.nestedModal}>
      <ModalBody className={styles.warningModal}>
        <img
          src="https://storage.googleapis.com/dechea-microservices-files/patient-management/polygon.svg"
          className={styles.polygon}
          alt="polygon"
        />
        <p className={styles.warningModalTitle}>Save Changes?</p>
        <p className={styles.warningModalBody}>
          You haven’t saved your changes yet, so you will lose them if you leave
          the page.
        </p>
        <div className={styles.nestedButtonClass}>
          <Button
            color="primary"
            className={styles.nestedButton}
            onClick={nestedToggle}
          >
            Yes
          </Button>{" "}
          <Button
            color="primary"
            className={styles.nestedButton}
            onClick={toggleAll}
          >
            No
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};

export default NestedModal;
