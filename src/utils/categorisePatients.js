const categorisePatients = (patients) => {
  if (!(patients && patients.length)) {
    return {
      todaysPatients: [],
      noAppointmentPatients: [],
      archivedPatients: [],
      allPatients: [],
    };
  }

  const allPatients = patients.filter(
    (patient) =>
      patient.patients &&
      patient.patients.length &&
      !patient.patients[0].archived
  );

  const todaysPatients = allPatients.filter(
    (patient) =>
      patient &&
      new Date(patient.patients[0].lastAppointment).setHours(0, 0, 0, 0) -
        new Date().setHours(0, 0, 0, 0) ===
        0 &&
      patient.status !== "NOT_ARRIVED"
  );

  const noAppointmentPatients = allPatients.filter(
    (patient) =>
      (patient && patient.patients[0].lastAppointment) ||
      new Date(patient && patient.patients[0].lastAppointment) - new Date() < 0
  );

  const archivedPatients = patients.filter(
    (patient) =>
      patient &&
      patient.patients &&
      patient.patients.length &&
      patient.patients[0].archived
  );

  return {
    todaysPatients,
    noAppointmentPatients,
    archivedPatients,
    allPatients,
  };
};

export default categorisePatients;
