import React from "react";
import { render } from "@testing-library/react";
import SearchLocationInput from "./SearchLocationInput";
import GuardianSearchLocationInput from "./GuardianSearchLocationInput";
import { mockGoogle } from "../../utils/mockGoogle";

window.google = global.google = mockGoogle();

it("test autocomplete", async () => {
  const { getByPlaceholderText } = render(
    <SearchLocationInput value={""} updateAddress={() => {}} user={""} />
  );
  getByPlaceholderText("Street");
});

it("test autocomplete", async () => {
  const { getByPlaceholderText } = render(
    <GuardianSearchLocationInput
      value={""}
      updateAddress={() => {}}
      user={""}
    />
  );
  getByPlaceholderText("Street");
});
