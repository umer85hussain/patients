import formateDate from "./formatDate";

export default function getTimeDifference(currentTime, futureTime) {
  const timeString = futureTime
    .toLocaleTimeString()
    .split(":")
    .slice(0, -1)
    .join(":");
  const diffTime =
    futureTime.setHours(0, 0, 0, 0) - currentTime.setHours(0, 0, 0, 0);
  const dayDifference = diffTime / (1000 * 60 * 60 * 24);
  if (dayDifference < 0) return formateDate(futureTime);
  if (dayDifference < 1) return `Today ${timeString}`;
  if (dayDifference < 2) return `Tomorrow ${timeString}`;
  return formateDate(futureTime);
}
