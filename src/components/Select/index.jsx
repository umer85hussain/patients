import React from "react";
import ReactSelect, { components } from "react-select";
import PropTypes from "prop-types";

import styles from "./select.module.scss";

const { Option } = components;

const IconOption = (props) => (
  <Option {...props}>
    <div className={styles.optionWrapper}>
      <div
        className={styles.circle}
        style={{ backgroundColor: props.data.color }}
      ></div>
      <div className={styles.label}>{props.data.label}</div>
    </div>
  </Option>
);

const Select = ({ value, options, handleChange, style, label, disable }) => {
  return (
    <ReactSelect
      isDisabled={disable}
      options={options}
      components={{ Option: IconOption, IndicatorSeparator: () => null }}
      styles={style}
      onChange={handleChange}
      value={{
        value: value ? value : "NOT_PRESENT",
        label: label ? label : "Not present",
      }}
      isSearchable={false}
    />
  );
};

Select.propTypes = {
  value: PropTypes.string,
  options: PropTypes.array,
  handleChange: PropTypes.func,
  style: PropTypes.object,
  label: PropTypes.string,
};

export default Select;
