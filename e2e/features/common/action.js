const { baseURL } = require("../../config");
const helper = require("./browser");

const action = {
  navigateToPage: function () {
    return new Promise((resolve, reject) => {
      this.driver.get(baseURL);
      resolve();
      reject();
    });
  },

  enterInput: function () {
    return helper(this.driver).waitAndLocateByCSS(itemSelector).sendKeys(value);
  },

  confirmTextVisibility: async function () {
    return new Promise((resolve, reject) => {
      helper(this.driver).waitAndLocateByXpath(`//table/tbody/tr`);
      resolve();
      reject();
    });
  },

  findPatients: async function () {
    return new Promise((resolve, reject) => {
      helper(this.driver).waitAndLocateByXpath(
        `//*[@id="patient_table"]/tbody/tr[1]`
      );
      resolve();
      reject();
    });
  },

  findOverview: async function () {
    return new Promise((resolve, reject) => {
      helper(this.driver)
        .waitAndLocateByXpath(`//*[@id="patient_table"]/tbody/tr[1]/td[1]`)
        .click();

      helper(this.driver).waitAndLocateByXpath(
        `/html/body/div[2]/div/div[1]/div/div/div[1]/h5/div`
      );
      resolve();
      reject();
    });
  },

  deletePatient: async function () {
    return new Promise((resolve, reject) => {
      helper(this.driver)
        .waitAndLocateByXpath(`//table/tbody/tr[0]/td[5]`)
        .click()
        .then(() => {
          helper(this.driver)
          .waitAndLocateByXpath(
            `//*[@id="patient_table"]/tbody/tr[1]/td[6]/div/div[2]/div[2]`
          )
          helper(this.driver)
            .waitAndLocateByXpath(
              `//*[@id="patient_table"]/tbody/tr[1]/td[6]/div/div[2]/div[2]`
            )
            .click();
        })
        .then(() => {
          resolve();
          reject();
        });
    });
  },
};

module.exports = action;
