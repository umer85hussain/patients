const isValidEmailAddress = (value) => {
  return (
    null !==
    value.match(/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/)
  );
};
const isValidName = (value) => {
  const re = /^[a-zA-ZäüöïëÄÜÖßáéíóúàèìùòâêîôûçÇãñõÆØÅæøå. ]+(?:-[a-zA-Z ]+)*$/;
  return re.test(value);
};
const isValidDay = (value) => {
  const re = /^[0-9]{1,2}$/;
  return re.test(value);
};
const isValidYear = (value) => {
  const re = /^[0-9]{4}$/;
  return re.test(value);
};
const isValidPhoneNumber = (value) => {
  return null !== value.match(/^[\s\d()+\/-]{10,20}$/);
};
const isValidPostalCode = (value) => {
  return null !== value.match(/^[\s\d()\/-]{5,6}$/);
};
const alphaNumPuncStrRegEx = /^[ \wäüöïëÄÜÖßáéíóúàèìùòâêîôûçÇãñõÆØÅæøå()/.,:+&-]*$/;
const isAlphaNumStr = (value) => {
  return null != value.match(alphaNumPuncStrRegEx);
};

export {
  isValidEmailAddress,
  isValidName,
  isValidDay,
  isValidYear,
  isValidPhoneNumber,
  isValidPostalCode,
  isAlphaNumStr,
};
