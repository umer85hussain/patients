const { Selector } = require("testcafe");
const config = require("../../../config");
// Selectors

function select(selector) {
  return Selector(selector).with({ boundTestRun: testController });
}

exports.patientmanagement = {
  url: function () {
    return config.baseURL;
  },
  rootElement: function () {
    return select("#root");
  },
  patientTable: function () {
    return Selector("table")
      .with({ boundTestRun: testController })
      .withAttribute("id", "patient_table");
  },
  addButton: function () {
    return Selector("button")
      .with({ boundTestRun: testController })
      .withText("New Patient");
  },
  saveButton: function () {
    return Selector("button")
      .with({ boundTestRun: testController })
      .withText("Save");
  },
  createButton: function () {
    return Selector("button")
      .with({ boundTestRun: testController })
      .withText("Create");
  },
  tableEntries: function () {
    return Selector("td")
      .with({ boundTestRun: testController })
      .withText("Peter Parker");
  },
  selectGender: function () {
    return select("#newGender");
  },
  inputFieldName: function () {
    return select("#inputFieldName");
  },
  inputFieldBirthDay: function () {
    return select("#inputFieldBirthDay");
  },
  inputFieldBirthMonth: function () {
    return select("#inputFieldBirthMonth");
  },
  inputFieldBirthYear: function () {
    return select("#inputFieldBirthYear");
  },
  inputFieldStreet: function () {
    return select("#inputFieldStreet");
  },
  inputFieldStreetNumber: function () {
    return select("#inputFieldStreetNumber");
  },
  inputFieldPostalCode: function () {
    return select("#inputFieldPostalCode");
  },
  inputFieldCity: function () {
    return select("#inputFieldCity");
  },
  inputFieldCountry: function () {
    return select("#inputFieldCountry");
  },
  inputFieldPhone: function () {
    return select("#inputFieldPhone");
  },
  inputFieldEmail: function () {
    return select("#inputFieldEmail");
  },
  inputFieldProfession: function () {
    return select("#inputFieldProfession");
  },
  inputFieldFamilyDoctor: function () {
    return select("#inputFieldFamilyDoctor");
  },
  inputFieldCareLevel: function () {
    return select("#inputFieldCareLevel");
  },
  inputInsuranceLabel: function () {
    return select("label")
      .with({ boundTestRun: testController })
      .withText("Legal");
  },
  inputFieldLegalInsuranceNumber: function () {
    return select("#inputFieldLegalInsuranceNumber");
  },
  firstSearchResult: function () {
    return Selector(".repo-list-item")
      .nth(0)
      .with({ boundTestRun: testController });
  },
  toggleMenu: function () {
    return Selector("#patient_table")
      .with({ boundTestRun: testController })
      .find(".toggleMenu_dots__2Cz4g");
  },
  toggleArchive: function () {
    return Selector("#patient_table")
      .with({ boundTestRun: testController })
      .find("div")
      .withText("Archive")
      .nth(3);
  },
  inputLegalGuardianLabel: function () {
    return select("label")
      .with({ boundTestRun: testController })
      .withText("Legal Guardian");
  },
  inputFieldLegalGuardianName: function () {
    return select("#inputFieldLegalGuardianName");
  },
  inputFieldLegalGuardianStreet: function () {
    return select("#inputFieldLegalGuardianStreet");
  },
  inputFieldLegalGuardianStreetNumber: function () {
    return select("#inputFieldLegalGuardianStreetNumber");
  },
  inputFieldLegalGuardianPostalCode: function () {
    return select("#inputFieldLegalGuardianPostalCode");
  },
  inputFieldLegalGuardianCity: function () {
    return select("#inputFieldLegalGuardianCity");
  },
  inputFieldLegalGuardianCountry: function () {
    return select("#inputFieldLegalGuardianCountry");
  },
  inputFieldLegalGuardianPhone: function () {
    return select("#inputFieldLegalGuardianPhone");
  },
  inputFieldLegalGuardianEmail: function () {
    return select("#inputFieldLegalGuardianEmail");
  },
};
