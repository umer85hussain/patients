import React, { useState } from "react";
import ReactSelect from "react-select";

import { SELECT_TITLE_STYLE } from "../../constants";
import styles from "./dropdownMenu.module.scss";
import inputStyles from "../FormGroupInput/formGroupInput.module.scss"

const DropdownMenu = ({
  id,
  options,
  value,
  handleChange,
  disable,
  label,
  sort,
  requiredError,
}) => {
  const [listOptions, setListOptions] = useState(options);

  const changeOptionsSequence = (selectedValue) => {
    let afterSelectedOptions = [];
    let beforeSelectedOptions = [];
    options.forEach((element) => {
      element.value >= selectedValue
        ? afterSelectedOptions.push(element)
        : beforeSelectedOptions.push(element);
    });
    setListOptions(afterSelectedOptions.concat(beforeSelectedOptions));
  };
  return (
    <div className={styles.scrollbar}>
      <ReactSelect
        id={id}
        components={{ IndicatorSeparator: () => null }}
        isSearchable={false}
        styles={SELECT_TITLE_STYLE}
        options={listOptions}
        value={value}
        onChange={(selected) => {
          sort && changeOptionsSequence(selected.value);
          handleChange(selected);
        }}
        isDisabled={disable}
        placeholder={label}
      />
      {!!requiredError && (
        <div
          className={
            requiredError ? (id === "newGender" ? styles.titleFeedback :styles.feedbackForm) : inputStyles.feedbackFormNone
          }
        >
          {Object.values(requiredError)[0]}
        </div>
      )}
    </div>
  );
};

export default DropdownMenu;
