import categorisePatients from './categorisePatients';
import formatDate from './formatDate';
import getTimeDifference from './getTimeDifference';

it("test formateDate function", () => {
  const getTimeDifferenceMock = jest.fn(date => formatDate(date))
  getTimeDifferenceMock("2000-01-23")
  expect(getTimeDifferenceMock.mock.calls.length).toBe(1);
  expect(getTimeDifferenceMock.mock.results[0].value).toStrictEqual("23, January, 2000");
})

it("test getTimeDifference function for Today", () => {
  const formatDateMock = jest.fn((currentDate, lastDate) => getTimeDifference(currentDate, lastDate))
  formatDateMock(new Date(), new Date())
  expect(formatDateMock.mock.calls.length).toBe(1);
  expect(formatDateMock.mock.results[0].value).toMatch('Today');
})

it("test getTimeDifference function for Tomorrow", () => {
  const formatDateMock = jest.fn((currentDate, lastDate) => getTimeDifference(currentDate, lastDate))
  const today = new Date()
  const tomorrow = new Date(today)
  tomorrow.setDate(tomorrow.getDate() + 1)
  formatDateMock(today, tomorrow)
  expect(formatDateMock.mock.calls.length).toBe(1);
  expect(formatDateMock.mock.results[0].value).toMatch('Tomorrow');
})

const categorisePatientResult = {
  "allPatients": [],
  "archivedPatients": [],
  "noAppointmentPatients": [],
  "todaysPatients": [],
}

const testUserData = [
  {
    firstName: "Franky",
    lastName: "Stiller",
    titel: "Prof.",
    birthdate: "2000-01-23",
    phone: "+49 831 54091090",
    patientSince: "2000-01-23",
    insurence: [
      { name: "AOK", id: 6, type: "private" },
      { name: "AOK", id: 6, type: "private" },
    ],
    address: {
      street: "Kottenerstr.",
      streetNo: "80",
      postalCode: "87435",
      city: "Kempten",
      country: "Germany",
    },
    id: 0,
    job: "Manager",
    lastAppointment: "2000-01-23",
    insurenceId: "BZT301381240",
    eMail: "franky.stiller@test.com",
  },
  {
    eMail: "JoanCyrus@gmail.com",
    firstName: "Joan",
    lastName: "Cyrus",
    insurenceId: "INS5244",
    address: {
      street: "Kottenerstr.",
      streetNo: "80",
      postalCode: "87435",
      city: "Kempten",
      country: "Germany",
    },
    appointmentDate: "09-25-2020 22:56:36",
    status: "Not present",
    dateOfBirth: "10-11-1992",
  },
  {
    eMail: "PeterMorrison@gmail.com",
    firstName: "Peter",
    lastName: "Morrison",
    insurenceId: "INS1286",
    address: {
      street: "Kottenerstr.",
      streetNo: "80",
      postalCode: "87435",
      city: "Kempten",
      country: "Germany",
    },
    appointmentDate: "09-26-2020 05:56:6",
    status: "Treatment",
    dateOfBirth: "01-10-1990",
  },
];

it("test categorisePatient function", () => {
  const categoriseMock = jest.fn(list => categorisePatients(list))
  categoriseMock(testUserData)
  expect(categoriseMock.mock.calls.length).toBe(1);
  expect(categoriseMock.mock.results[0].value).toStrictEqual(categorisePatientResult);
})
