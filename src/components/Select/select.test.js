import React from "react";
import { render } from "@testing-library/react";
import Select from "./index";
import { STATUS } from "../../constants";

it("test select option changes", () => {
  const { getByText } = render(
    <Select value={STATUS[0].value} label={STATUS[0].label} />
  );
  getByText("Waitroom");
});
