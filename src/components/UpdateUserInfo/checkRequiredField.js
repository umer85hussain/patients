import { camelCaseToNormal } from "../../utils/formatText";

export const checkRequiredFields = (fields) => {
  const errors = [];
  const {
    archived,
    address,
    degreeOfCare,
    firstName,
    lastName,
    email,
    profession,
    familyDoctor,
    gender,
    street,
    insuranceNumber,
    ...restFields
  } = fields;

  Object.keys(restFields).forEach((fieldName) => {
    if (!restFields[fieldName]) {
      errors.push({
        [fieldName]: `${camelCaseToNormal(fieldName)} is required`,
      });
    }
  });
  // Object.keys(address).forEach((fieldName) => {
  //   if (!address[fieldName] && fieldName !== "archived") {
  //     errors.push({
  //       [fieldName]: `${camelCaseToNormal(fieldName)} is required`,
  //     });
  //   }
  // });
  if (!firstName.trim().length) {
    errors.push({ firstName: `${camelCaseToNormal("firstName")} is required` });
  }
  if (!lastName.trim().length) {
    errors.push({ lastName: `${camelCaseToNormal("lastName")} is required` });
  }
  return errors;
};
