Feature: Administrate all my patients

  Scenario: Patient wants his patient records deleted
    Given Carmen is logged in
    And There are existing patients
    When Carmen removes a patient
    Then All patient records for the patient are removed